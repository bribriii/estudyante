const makeCourseUpdateEntity = ({}) => {
    return function makeCourse({courseid, coursedesc, yearcommenced}){
        if(!courseid){
            throw new Error("course id required");
        }
        if(!coursedesc){
            throw new Error("course description required");
        }

        return Object.freeze({
            getCourseId: () => courseid,
            getCourseDesc: () => coursedesc,
        });

    }
}

module.exports = makeCourseUpdateEntity;