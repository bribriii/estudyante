const makeCourseEntity = ({}) => {
    return function makeCourse({courseid, coursedesc, yearcommenced}){
        let _yearcommenced = yearcommenced
        if(!courseid){
            throw new Error("course id required");
        }
        if(!coursedesc){
            throw new Error("course description required");
        }
        if(!yearcommenced){
            _yearcommenced = new Date().getFullYear();
        }else{
            if(yearcommenced < new Date().getFullYear()){
                throw new Error("year cannot be older than current year");
            }    
        }

        return Object.freeze({
            getCourseId: () => courseid,
            getCourseDesc: () => coursedesc,
            getYearCommenced: () => _yearcommenced
        });

    }
}

module.exports = makeCourseEntity;