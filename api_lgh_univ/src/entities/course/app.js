const makeCourseEntity = require('./course');
const makeCourseUpdateEntity = require('./course-update');

const makeCourse = makeCourseEntity({});
const makeCourseUpdate = makeCourseUpdateEntity({})

const courseEntity = Object.freeze({
    makeCourse,
    makeCourseUpdate
});


module.exports = courseEntity;
module.exports = {
    makeCourse,
    makeCourseUpdate
}