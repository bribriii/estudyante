const makeAttemptEntity = require('./attempt');
const makeAttemptGradeEntity = require('./attempt-graded');

const makeAttempt = makeAttemptEntity({});
const makeAttemptGrade = makeAttemptGradeEntity({});

const attemptEntity = Object.freeze({
    makeAttempt,
    makeAttemptGrade
});

module.exports = attemptEntity;
module.exports = {
    makeAttempt,
    makeAttemptGrade
}