const makeAttemptGradedEntity = ({}) => {
    return function makeAttempt({sy, semester, sid, sectionid, subjectid, mark}){
        let grade = '';
        if(!sy){
            throw new Error('school year requiired');
        }
        if(!semester){
            throw new Error('semester requiired');
        }
        if(!sid){
            throw new Error('student id requiired');
        }
        if(!sectionid){
            throw new Error('section requiired');
        }
        if(!subjectid){
            throw new Error('subject requiired');
        }
        if(!mark){
            throw new Error('mark required');
        }
        if(isNaN(mark)){
            throw new Error('mark should be a number');
        }

        return Object.freeze({
            getSy: () => sy,
            getSemester: () => semester,
            getSid: () => sid,
            getSectiondId: () => sectionid,
            getSubjectId: () => subjectid,
            getMark: () => mark,
            getGrade: () => getgrade(mark)

        })
    }

    function getgrade(m){
        if(m >= 95 && m <= 100)
            return 'A';
        if(m >= 89 && m <= 94)
            return 'B';
        if(m >= 83 && m <= 88)
            return 'C';
        if(m >= 75 && m <= 82)
            return 'D';
        if(m >= 0 && m <= 74)
            return 'F';
    }
}

module.exports = makeAttemptGradedEntity;