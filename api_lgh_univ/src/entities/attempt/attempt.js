const makeAttemptEntity = ({}) => {
    return function makeAttempt({sy, semester, sid, sectionid, subjectid}){
        if(!sy){
            throw new Error('school year requiired');
        }
        if(!semester){
            throw new Error('semester requiired');
        }
        if(!sid){
            throw new Error('student id requiired');
        }
        if(!sectionid){
            throw new Error('section requiired');
        }
        if(!subjectid){
            throw new Error('subject requiired');
        }
        return Object.freeze({
            getSy: () => sy,
            getSemester: () => semester,
            getSid: () => sid,
            getSectiondId: () => sectionid,
            getSubjectId: () => subjectid

        })
    }
}

module.exports = makeAttemptEntity;