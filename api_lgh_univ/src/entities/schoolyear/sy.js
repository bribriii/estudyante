const makeSyEntity = ({}) => {
    return function makeSy({sy, semester}){
        if(!sy){
            throw new Error("school year required required");
        }
        if(!semester){
            throw new Error("semester required");
        }

        return Object.freeze({
            getSy: () => sy,
            getSemester: () => semester
        });

    }
}

module.exports = makeSyEntity;