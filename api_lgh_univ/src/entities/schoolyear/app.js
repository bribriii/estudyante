const makeSyEntity = require('./sy');

const makeSy = makeSyEntity({});

const syEntity = Object.freeze({
    makeSy
});

module.exports = syEntity;
module.exports = {
    makeSy
}