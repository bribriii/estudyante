const makeSectionEntity = ({}) => {
    return function makeSection({sectionid, subjectid, classsize}){
        if(!subjectid){
            throw new Error('subject id required');
        }
        if(!sectionid){
            throw new Error('section id required');
        }
        if(!classsize){
            throw new Error('class size required');
        }
        if(isNaN(classsize)){
            throw new Error('class size should be a number');
        }

        return Object.freeze({
            getSectionId: () => sectionid,
            getSubjectId: () => subjectid,
            getClassSize: () => classsize
        });
    }
}

module.exports = makeSectionEntity;