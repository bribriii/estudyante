const makeSectionEntity = require('./section');

const makeSection = makeSectionEntity({});

const sectionEntity = Object.freeze({
    makeSection
});

module.exports = sectionEntity;
module.exports = {
    makeSection
}