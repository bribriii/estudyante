const makeSubjectEntity = ({}) => {
    return function makeSubject({subjectid, subjectdesc, creditpoints}){
        if(!subjectid){
            throw new Error('subject id required');
        }
        if(!subjectdesc){
            throw new Error('subject description required');
        }
        if(!creditpoints){
            throw new Error('credit points required');
        }

        return Object.freeze({
            getSubjectId: () => subjectid,
            getSubjectDesc: () => subjectdesc,
            getCreditPoints: () => creditpoints
        });
    }
}

module.exports = makeSubjectEntity;