const makeSubjectUpdateEntity = ({}) => {
    return function makeSubject({subjectid, subjectdesc, creditpoints}){
        if(!subjectid){
            throw new Error('subject id required');
        }
        
        return Object.freeze({
            getSubjectId: () => subjectid,
            getCourseDesc: () => subjectdesc,
            getCreditPoints: () => creditpoints
        });
    }
}

module.exports = makeSubjectUpdateEntity;