const makeSubjectEntity = require('./subject');
const makeSubjectUpdateEntity = require('./subject-update');

const makeSubject = makeSubjectEntity({});
const makeSubjectUpdate = makeSubjectUpdateEntity({});

const subjectEntity = Object.freeze({
    makeSubject,
    makeSubjectUpdate
});

module.exports = subjectEntity;