const makeAuthEntity = ({}) => {
    return function makeAccount({uid, password}, tag){
        if(!uid ){
            throw new Error("user id required");
        }
        if(!password){
            throw new Error("password required");
        }
    

        return Object.freeze({
            getUid: () => uid,
            getPassword: () => password
        })
    }
}

module.exports = makeAuthEntity;