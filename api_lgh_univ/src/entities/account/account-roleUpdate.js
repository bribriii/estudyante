const makeAccountUpdateEntity = ({}) => {
    return function makeAccount({uid, password, roleid, active}){
        if(!uid ){
            throw new Error("user id required");
        }
        if(!roleid){
            throw new Error("role id required")
        }
        if(isNaN(roleid)){
            throw new Error("role id must be a number");
        }
    
        return Object.freeze({
            getUid: () => uid,
            getRoleId: () => roleid
        })
    }
}

module.exports = makeAccountUpdateEntity;