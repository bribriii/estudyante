const makeAccountEntity = require("./account");
const makeAuthEntity = require('./account-auth');
const makeAccRoleUpdate = require('./account-roleUpdate');

const makeAccount = makeAccountEntity({});
const makeAuth = makeAuthEntity({});
const makeAcctRoleUpdate = makeAccRoleUpdate({});

const accountEntity = Object.freeze({
    makeAccount,
    makeAuth,
    makeAcctRoleUpdate
});

module.exports = accountEntity;
module.exports = {
    makeAccount,
    makeAuth,
    makeAcctRoleUpdate
};