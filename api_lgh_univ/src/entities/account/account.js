const makeAccountEntity = ({}) => {
    return function makeAccount({uid, password, roleid, active}){
        if(!uid ){
            throw new Error("user id required");
        }
        if(!password){
            throw new Error("password required");
        }
        if(!roleid){
            throw new Error("role id required")
        }
        if(isNaN(roleid)){
            throw new Error("role id must be a number");
        }
    
        return Object.freeze({
            getUid: () => uid,
            getPassword: () => password,
            getRoleId: () => roleid,
            getActive: () => active
        })
    }
}

module.exports = makeAccountEntity;