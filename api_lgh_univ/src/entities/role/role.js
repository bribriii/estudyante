const makeRoleEntity = ({}) => {
    return function makeRole({ id, roledesc, active } = {}) {
    
      return Object.freeze({
        getId: () => id,
        getRoleDesc: () => roledesc,
        getActive: () => active
      });
    };
  };
  
  module.exports = makeRoleEntity;
  