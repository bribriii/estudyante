const makeRoleEntity = require('./role');
const makeRoleUpdateEntity = require('./role-update');

const makeRole = makeRoleEntity({});
const makeRoleUpdate = makeRoleUpdateEntity({});

const roleEntity = Object.freeze({
    makeRole,
    makeRoleUpdate
})

module.exports = roleEntity;

module.exports = {
    makeRole,
    makeRoleUpdate
};