const makeRoleUpdateEntity = ({}) => {
    return function makeRole({ id, roledesc, active } = {}) {
      
      if (!roledesc) {
        throw new Error("Role description required");
      }
      if(isNaN(id)){
        throw new Error("role id should be a number");
      }
        
      return Object.freeze({
        getId: () => id,
        getRoleDesc: () => roledesc,
        getActive: () => active
      });
    };
  };
  
  module.exports = makeRoleUpdateEntity;