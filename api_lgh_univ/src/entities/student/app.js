const makeStudentEntity = require('./student');
const makeStudentUpdateEntity = require('./student-update');

const makeStudent = makeStudentEntity({});
const makeStudentUpdate = makeStudentUpdateEntity({});

const studentEntity = Object.freeze({
    makeStudent,
    makeStudentUpdate
});

module.exports  = studentEntity;
module.exports = {
    makeStudent,
    makeStudentUpdate
};