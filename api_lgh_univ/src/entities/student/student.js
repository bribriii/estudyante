const makeStudentEntity = ({}) => {
    return function makeStudent({givenname, surname, dob, yearenrolled, yearlevel, courseid}){
        var _yearenrolled = yearenrolled

        // if(!sid){
        //     throw new Error('student id required');
        // }
        if(!givenname){
            throw new Error('first name required');
        }
        if(!surname){
            throw new Error('last name required');
        }
        if(!dob){
            throw new Error('date of birth required');
        }
        if(!yearenrolled){
            _yearenrolled = new Date().getFullYear();
        }
        if(yearenrolled > new Date().getFullYear()){
            throw new Error('invalid year of enrollment');
        }
        if(!yearlevel){
            throw new Error('year level required');
        }
        if(!courseid){
            throw new Error('student should be enrolled in a course');
        }

        return Object.freeze({
            getSid: () => sid,
            getGivenName: () => givenname,
            getSurname: () => surname,
            getDob: () => dob,
            getYearEnrolled: () => _yearenrolled,
            getYearLevel: () => yearlevel,
            getCourse: () => courseid
        })
    }
    
}

module.exports = makeStudentEntity;