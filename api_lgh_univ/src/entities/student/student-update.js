const makeStudentEntity = ({}) => {
    return function makeStudent({sid, givenname, surname, dob, yearlevel, courseid}){

        if(!sid){
            throw new Error('student id required');
        }

        return Object.freeze({
            getSid: () => sid,
            getGivenName: () => givenname,
            getSurname: () => surname,
            getDob: () => dob,
            getYearLevel: () => yearlevel,
            getCourse: () => courseid
        })
    }
    
}

module.exports = makeStudentEntity;