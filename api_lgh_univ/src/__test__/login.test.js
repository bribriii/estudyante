const supertest = require('supertest');
const app = require('../app');

const request = supertest.agent(app);


describe('TEST SUITE FOR STUDENT FUNCTIONS', () => {
    //login with correct credentials
    describe('/login', () => {
        it('should successfully log in to the system, given the right student credentials', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    uid: '2015-0448',
                    password: 'letmein123'
                })
                .catch(err => console.log(err))
                expect(res.status).toEqual(200);
              //  expect(res.body).toHaveProperty('success', true)
                done();
        });
    });

    //login with the wrong password
    describe('/login', () => {
        it('should not log in with wrong password', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    uid: '2015-0448',
                    password: 'wrongpassword'
                })
                .catch(err => console.log(err))

                expect(res.status).toEqual(400);
              //  expect(res.body).toHaveProperty('success', false)
                done();
        });
    });

    //login without uid
    describe('/login', () => {
        it('should not log in without uid', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    uid: '',
                    password: 'letmein123'
                })
                .catch(err => console.log(err))

                expect(res.status).toEqual(400);
              //  expect(res.body).toHaveProperty('success', false)
                done();
        });
    });

    //login without password
    describe('/login', () => {
        it('should not log in without password', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    uid: '2015-0448',
                    password: ''
                })
                .catch(err => console.log(err))

                expect(res.status).toEqual(400);
               // expect(res.body).toHaveProperty('success', false)
                done();
        });
    });

    //login with undefined uid
    describe('/login', () => {
        it('should not log in with undefined uid', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    password: 'letmein123'
                })
                .catch(err => console.log(err))

                expect(res.status).toEqual(400);
              // expect(res.body).toHaveProperty('success', false)
                done();
        });
    });

    //login with undefined password
    describe('Test for student log in route: /login', () => {
        it('should not log in with undefined password', async done => {
            const res = await supertest(app)
                .post('/login')
                .send({
                    uid: '2015-0448'
                })
                .catch(err => console.log(err))
                expect(res.status).toEqual(400);
              //  expect(res.body).toHaveProperty('success', false);
                done();
        });
    });

  });
