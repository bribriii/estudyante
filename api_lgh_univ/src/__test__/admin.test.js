const supertest = require('supertest');
const app = require('../app');

const request = supertest.agent(app);


let loginData ={};

beforeAll(async () => {    
    loginData =  await request
            .post('/login')
            .send({
                uid: '2015-0000',
                password: 'letmein123'
            })
            .set('Accept', 'application/json');
    });



    describe('TEST SUITE FOR ADMIN FUNCTIONS', () => {

        //account
        describe('Test route:  /api/accounts/updatepass', () => {
            test('Admin can Change his/her Password', async done => {
                const res = await supertest(app)
                    .post('/api/accounts/updatepass')
                    .send({
                        uid: '2015-0000',
                        password: 'letmein123'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })
                    expect(res.statusCode).toBe(201);
                    expect(res.body.message).toBe('password updated successfully');
                    done();
            });
        });

        describe('Test route:  /api/accounts/newaccount', () => {
            test('Admin can Add User', async done => {
                const res = await supertest(app)
                    .post('/api/accounts/newaccount')
                    .send({
                        roleid: 1
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        describe('Test route:  /api/accounts/updaterole', () => {
            test('Admin can Add User', async done => {
                const res = await supertest(app)
                    .post('/api/accounts/updaterole')
                    .send({
                        uid: '2015-0000',
                        roleid: 3
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route:  /api/accounts/resetpass', () => {
            test('Admin can Reset User\'s Password to default', async done => {
                const res = await supertest(app)
                    .post('/api/accounts/resetpass')
                    .send({
                        uid: '2015-0000'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        describe('Test route: /api/accounts/deactivate', () => {
            test('Admin can Deactivate User Account', async done => {
                const res = await supertest(app)
                    .post('/api/accounts/deactivate')
                    .send({
                        uid: '2015-0000'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })
                    expect(res.statusCode).toBe(201);

                    await supertest(app)
                    .post('/api/accounts/activate')
                    .send({
                        uid: '2015-0000'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    })

                    done();
            });
        });

        describe('Test route: /api/accounts/list', () => {
            test('Admin can view User Details', async done => {
                const res = await supertest(app)
                    .get('/api/accounts/list')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        //COURSES
        describe('Test route: /api/courses/add', () => {
            test('Admin can Add Course', async done => {
                const res = await supertest(app)
                    .post('/api/courses/add')
                    .send({
                        courseid: 'BSBIO', 
                        coursedesc: 'BS in Biology'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        describe('Test route: /api/courses/:courseid', () => {
            test('Admin can Update Course Details', async done => {
                const res = await supertest(app)
                    .put('/api/courses/BSBIO')
                    .send({
                        coursedesc: 'BS in Biological Science'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route: /api/courses/list', () => {
            test('Admin can view Course Details', async done => {
                const res = await supertest(app)
                    .get('/api/courses/list')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route: /api/courses/:courseid', () => {
            test('Admin can Deactivate Course', async done => {
                const res = await supertest(app)
                    .delete('/api/courses/BSBIO')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        //SUBJECTS
        describe('Test route: /api/subjects/add', () => {
            test('Admin can Add Subjects', async done => {
                const res = await supertest(app)
                    .post('/api/subjects/add')
                    .send({
                        subjectid: 'CSC100', 
                        subjectdesc: 'Computer Programming I', 
                        creditpoints: 3
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        describe('Test route: /api/subjects/:subjectid', () => {
            test('Admin can Update Subject Details', async done => {
                const res = await supertest(app)
                    .put('/api/subjects/CSC100')
                    .send({ 
                        subjectdesc: 'Computer Programming I', 
                        creditpoints: 4
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route: /api/subjects/:subjectid', () => {
            test('Admin can Deactivate Subject', async done => {
                const res = await supertest(app)
                    .delete('/api/subjects/CSC100')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        describe('Test route:  /api/subjects/list', () => {
            test('Admin can view Subject Details', async done => {
                const res = await supertest(app)
                    .get('/api/subjects/list')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        //ROLE
        // describe('Test route: /api/role/add', () => {
        //     test('Admin can Add Role', async done => {
        //         const res = await supertest(app)
        //             .post('/api/role/add')
        //             .send({
        //                 roledesc: 'Staff'
        //             })
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             });
        //             console.log(res.body);
        //             expect(res.statusCode).toBe(201);
        //             done();
        //     });
        // });

        // describe('Test route: /api/role/:roleid', () => {
        //     test('Admin can Update user Role', async done => {
        //         const res = await supertest(app)
        //             .put('/api/role/1')
        //             .send({
        //                 roledesc: 'Student'
        //             })
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             });
        //             expect(res.statusCode).toBe(200);
        //             done();
        //     });
        // });

        // describe('Test route: /api/role/:roleid', () => {
        //     test('Admin can Deactivate Role', async done => {
        //         const res = await supertest(app)
        //             .delete('/api/role/1')
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             });
        //             expect(res.statusCode).toBe(200);
        //             done();
        //     });
        // });

        // describe('Test route: /api/role/:roleid', () => {
        //     test('Admin can view Role Details', async done => {
        //         const res = await supertest(app)
        //             .get('/api/role/1')
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             });
        //             expect(res.statusCode).toBe(200);
        //             done();
        //     });
        // });

        describe('Test route: /api/sections/add', () => {
            test('Admin can Add Section', async done => {
                const res = await supertest(app)
                    .post('/api/sections/add')
                    .send({
                        sectionid: 'C', 
                        subjectid: 'MATH17', 
                        classsize: 35
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });
        
        describe('Test route: /api/sections/update', () => {
            test('Admin can Update Section', async done => {
                const res = await supertest(app)
                    .put('/api/sections/update')
                    .send({
                        sectionid: 'A', 
                        subjectid: 'MATH17', 
                        classsize: 45
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route: /api/sections/deactivate', () => {
            test('Admin can Deactivate Section', async done => {
                const res = await supertest(app)
                    .post('/api/sections/deactivate')
                    .send({
                        sectionid: 'C', 
                        subjectid: 'MATH17'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        describe('Test route: /api/sections/list', () => {
            test('Admin can view Section Details', async done => {
                const res = await supertest(app)
                    .get('/api/sections/list')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);
                    done();
            });
        });

        //SCHOOL YEAR
        describe('Test route: /api/sy/add', () => {
            test('Admin can Add School Year', async done => {
                const res = await supertest(app)
                    .post('/api/sy/add')
                    .send({
                        sy: '2019-2020', 
                        semester: '2'
                    })
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(201);
                    done();
            });
        });

        // describe('Test route: /api/sy/deactivate', () => {
        //     test('Admin can Deactivate School Year', async done => {
        //         const res = await supertest(app)
        //             .delete('/api/sy/deactivate')
        //             .send({
        //                 sy: '2019-2020', 
        //                 semester: '2'
        //             })
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             });
        //             expect(res.statusCode).toBe(200);

        //             await supertest(app)
        //             .post('/api/sy/activate')
        //             .send({
        //                 sy: '2019-2020', 
        //                 semester: '2'
        //             })
        //             .set({
        //                 authorization: 'Bearer ' + loginData.body.token
        //             })

        //             done();
        //     });
        // });

        describe('Test route: /api/sy/list', () => {
            test('Admin can view School Year details', async done => {
                const res = await supertest(app)
                    .get('/api/sy/list')
                    .set({
                        authorization: 'Bearer ' + loginData.body.token
                    });
                    expect(res.statusCode).toBe(200);

                    
                    done();
            });
        });

    });