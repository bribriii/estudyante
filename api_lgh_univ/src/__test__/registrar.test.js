const supertest = require('supertest');
const app = require('../app');

const request = supertest.agent(app);

let loginData = {};

beforeAll(async () => {
    loginData = await request
                    .post('/login')
                    .send({
                        uid: '2015-0001',
                        password: 'letmein123'
                    })
                    .set('Accept', 'application/json');
})


describe('TEST SUITE FOR REGISTRAR FUNCTIONS', () => {

    //account
    describe('Test route:  /api/accounts/updatepass', () => {
        test('Registrar can Change his/her Password', async done => {
            const res = await supertest(app)
                .post('/api/accounts/updatepass')
                .send({
                    uid: '2015-0001',
                    password: 'letmein123'
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                console.log(res.body)
                expect(res.statusCode).toBe(201);
                //expect(res.body.message).toBe('password updated successfully');
                done();
        });
    });

    //student
    describe('Test route: /api/students/add', () => {
        test('Registrar can Add Student information', async done => {
            const res = await supertest(app)
                .post('/api/students/add')
                .send({
                    sid: '2015-0449',
                    givenname: 'Olivia Caroline', 
                    surname: 'Pope', 
                    yearlevel: 1, 
                    dob: '1998-03-19', 
                    courseid: 'BSIT'
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                console.log(res.body);
                expect(res.statusCode).toBe(201);
                done();
        });
    });

    describe('Test route: /api/students/:sid', () => {
        test(' Registrar can Update Student\'s information', async done => {
            const res = await supertest(app)
                .put('/api/students/2015-0448')
                .send({
                    givenname: 'Rey Brian',
                    surname: 'Astudillo',
                    dob: '1998-11-24',
                    yearenrolled: 2015,
                    courseid: 'BSIT',
                    yearlevel: 1
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                console.log(res.body)
                expect(res.statusCode).toBe(201);
                done();
        });
    });

    describe('Test route: /api/students/:sid', () => {
        test('Registrar can view Student Information', async done => {
            const res = await supertest(app)
                .get('/api/students/list')
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });

    //enrollment
    describe('Test route: /api/attempts/enroll', () => {
        test('Registrar can Add student\'s Subject ', async done => {
            const res = await supertest(app)
                .post('/api/attempts/enroll')
                .send({
                    sid: '2015-0448', 
                    subjectid: 'MATH17', 
                    sectionid: 'A', 
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(201);
                done();
        });
    });


    describe('Test route: /api/attempts/encodegrade', () => {
        test('Registrar can now encode', async done => {
            const res = await supertest(app)
                .post('/api/attempts/encodegrade')
                .send({
                    sid: '2015-0448', 
                    semester: '1', 
                    sy: '2019-2020', 
                    subjectid: 'MATH17', 
                    sectionid: 'A', 
                    mark: 88
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });

    describe('Test route: /api/attempts/listcurrent', () => {
        test('Registrar can view enrolled subjects', async done => {
            const res = await supertest(app)
                .get('/api/attempts/listcurrent')
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });




})