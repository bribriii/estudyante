const supertest = require('supertest');
const app = require('../app');

const request = supertest.agent(app);


let loginData ={};

beforeAll(async () => {    
    loginData =  await request
            .post('/login')
            .send({
                uid: '2015-0448',
                password: 'letmein123'
            })
            .set('Accept', 'application/json');
    });



describe('TEST SUITE FOR STUDENT FUNCTIONS', () => {

    describe('Test route: /api/students/:sid', () => {
        test('Student can view profile', async done => {
            const res = await supertest(app)
                .get('/api/students/2015-0448')
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });

    describe('Test route: /api/students/grade', () => {
        test('Student can View Grades', async done => {
            const res = await supertest(app)
                .post('/api/students/grade')
                .send({
                    sid: '2015-0448'
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });

    describe('Test route: /api/courses/curriculum/:courseid', () => {
        test('Student can View his/her Prospectus', async done => {
            const res = await supertest(app)
                .get('/api/courses/curriculum/BSIT')
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                done();
        });
    });

    describe('Test route: /api/accounts/updatepass', () => {
        test('Student can update his/her password', async done => {
            const res = await supertest(app)
                .post('/api/accounts/updatepass')
                .send({
                    uid: '2015-0448',
                    password: 'letmein123'
                })
                .set({
                    authorization: 'Bearer ' + loginData.body.token
                })
                expect(res.statusCode).toBe(200);
                expect(res.body.message).toBe('password updated successfully');
                done();
        });
    });

})