let jwt = require('jsonwebtoken');
const config = require('./config.js');


module.exports = {
    verifytoken(req, res, next){
        let token = req.headers['x-access-token'] || req.headers['authorization']

        
        if (token) {
            if (token.startsWith('Bearer ')) {
              token = token.slice(7, token.length);
            }
        
            jwt.verify(token, config.secret, (err, decoded) => {
              if (err) {
                return res.json({
                  success: false,
                  message: 'Token is not valid',
                  error: err.toString()
                });
              } else {
                req.decoded = decoded;
                next();
              }
            });
          } else {
            return res.json({
              success: false,
              message: 'Auth token is not supplied'
            });
          }
    },

    isAdmin(req,res,next){
        if(req.decoded.roleid === 3){
            next();
        }else {
            return res.json({
                success: false,
                message: 'Admin required'
            });
        }
    },

    isRegistrar(req,res,next){
        if(req.decoded.roleid === 2){
            next();
        }else {
            return res.json({
                success: false,
                message: 'Registrar required'
            });
        }
    },

    isNotStudent(req, res, next){
      if(req.decoded.roleid === 1){
        return res.json({
            success: false,
            message: 'unauthorized access'
        });
      } else{
        next()
      }
    },

    isStudent(req,res,next){
        if(req.decoded.roleid === 1){
            next();
        }else {
            return res.json({
                success: false,
                message: 'Student required'
            });
        }
    },

}