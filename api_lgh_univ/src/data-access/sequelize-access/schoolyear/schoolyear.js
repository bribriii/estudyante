const Schoolyear = require('../../models').Schoolyear;

const schoolyearDb = () => {
    return Object.freeze({
        listSy,
        findSy,
        addSy,
        getCurrentSy
    });

    async function listSy(){
        return Schoolyear
            .findAll()
    }

    async function findSy(syInfo){
        return Schoolyear
            .findAll({
                where: {
                    sy: syInfo.sy,
                    semester: syInfo.semester
                }
            });
    }

    async function addSy(syInfo){
        return Schoolyear
            .create({
                sy: syInfo.sy,
                semester: syInfo.semester,
                active: true
            });
    }

    async function getCurrentSy(){
        return Schoolyear.findOne({
            where: {active: true}
        });
    }
}

module.exports = schoolyearDb;














//     deac(req, res){
//         return Schoolyear
//         .findOne({
//             where: {
//                 sy: req.body.sy,
//                 semester: req.body.semester
//             }
//         })
//         .then(sy => {
//             if(!sy){
//                 return res.status(404).send({
//                     message: 'not found'
//                 });
//             }
//             return sy
//                 .update({
//                     active: false
//                 })
//                 .then(() => res.status(200).send(sy))
//                 .catch((err) => res.status(400).send(err));
//         })
//         .catch((err) => res.status(400).send(err));
//     },

//     activate(req, res){
//         return Schoolyear
//         .findOne({
//             where: {
//                 sy: req.body.sy,
//                 semester: req.body.semester
//             }
//         })
//         .then(sy => {
//             if(!sy){
//                 return res.status(404).send({
//                     message: 'not found'
//                 });
//             }
//             return sy
//                 .update({
//                     active: true
//                 })
//                 .then(() => res.status(200).send(sy))
//                 .catch((err) => res.status(400).send(err));
//         })
//         .catch((err) => res.status(400).send(err));
//     }

