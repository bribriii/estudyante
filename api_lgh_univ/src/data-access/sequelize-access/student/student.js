const Student = require('../../models').Student;
const Course = require('../../models').Course;
const Curriculum = require('../../models').Curriculum;
const Subjects = require('../../models').Subjects;
const Attempts = require('../../models').Attempt;
//const Joi = require('joi');


const studentDb = () => {
    return Object.freeze({
        listStudents,
        createStudent,
        findStudent,
        findStudentSubjects,
        findStudentGrades,
        updateStudentProfile
    });

    async function listStudents(){
        return Student
            .findAll({
                include:[{model: Course, as: 'course', attributes: ['courseid', 'coursedesc', 'yearcommenced']}],
                attributes: ['sid', 'givenname', 'surname', 'dob', 'yearenrolled', 'yearlevel']
        });
    }

    async function createStudent(studentInfo){
        return Student
            .create({
                sid: studentInfo.sid,
                givenname: studentInfo.givenname,
                surname: studentInfo.surname,
                yearenrolled: studentInfo.yearenrolled,
                yearlevel: studentInfo.yearlevel,
                dob: studentInfo.dob,
                courseid: studentInfo.courseid
        });
    }
    
    async function findStudent(sid){
        return Student
            .findByPk(sid,
                {include:[{model: Course, as: 'course', attributes: ['courseid', 'coursedesc', 'yearcommenced']}],
                attributes: ['sid', 'givenname', 'surname', 'dob', 'yearenrolled', 'yearlevel']
        });
    }

    async function findStudentSubjects(sid){
        return Student
            .findByPk(sid,
                {include:[{model: Course, as: 'course', attributes: ['courseid', 'coursedesc', 'yearcommenced'],
                    include: [
                        {
                            model: Curriculum, as: 'subjects', 
                            attributes:[
                                'subjectid', 
                                'year', 
                                'semester'
                            ],
                            include: [{
                                model: Subjects, as: 'subjectinfo', 
                                attributes:['subjectdesc', 'creditpoints']
                            }]
                        },
                    ]
                }],
                attributes: ['sid', 'givenname', 'surname', 'dob', 'yearenrolled', 'yearlevel'],
        });
    }

    async function findStudentGrades(sid){
        return Student
            .findByPk(sid,
                {include: [{model: Attempts, as: 'grades'}]}    
            )
    }

    async function updateStudentProfile(studentInfo){
        return Student
                .update({
                    givenname: studentInfo.givenname,
                    surname: studentInfo.surname,
                    yearlevel: studentInfo.yearlevel,
                    dob: studentInfo.dob,
                    courseid: studentInfo.courseid
                },{
                    where: {
                        sid: studentInfo.sid
                    }
                });
    }
}

module.exports = studentDb;