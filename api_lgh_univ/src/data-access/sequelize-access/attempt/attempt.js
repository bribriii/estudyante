const Attempt = require('../../models').Attempt;
const Student = require('../../models').Student;



const attemptDb = () => {
    return Object.freeze({
        listAttempts,
        listCurrentAttempts,
        findAttempt,
        createNewAttempt,
        encodeAttemptGrade
    });

    async function listAttempts(){
        return Attempt
            .findAll({ attributes: ['sy', 'semester', 'subjectid', 'sectionid', 'mark', 'grade'],
                include: [{model: Student, as: 'student', attributes: ['sid', 'givenname', 'surname', 'yearenrolled', 'yearlevel', 'courseid']}]
            })
    }

    async function listCurrentAttempts(attemptInfo){
            return Attempt
            .findAll({
                where:{
                    sy: attemptInfo.sy,
                    semester: attemptInfo.semester
                }, 
                attributes: ['sy', 'semester', 'subjectid', 'sectionid', 'mark', 'grade'],
                include: [{model: Student, as: 'student', attributes: ['sid', 'givenname', 'surname', 'yearenrolled', 'yearlevel', 'courseid']}]
            })
    }

    async function findAttempt(attemptInfo){
        return Attempt
            .findOne({
                where: {
                    sid: attemptInfo.sid,
                    semester: attemptInfo.semester, 
                    sy: attemptInfo.sy, 
                    subjectid: attemptInfo.subjectid, 
                    sectionid: attemptInfo.sectionid
                }
            });
    }

    async function createNewAttempt(attemptInfo){
            return Attempt
            .create({
                sid: attemptInfo.sid,
                sy: attemptInfo.sy,
                semester: attemptInfo.semester,
                sectionid: attemptInfo.sectionid,
                subjectid: attemptInfo.subjectid
            })
    }

    async function encodeAttemptGrade(attemptInfo){
            return Attempt
            .update({
                mark: attemptInfo.mark,
                grade: attemptInfo.grade
            },{
                where:{
                    sid: attemptInfo.sid,
                    semester: attemptInfo.semester, 
                    sy: attemptInfo.sy, 
                    subjectid: attemptInfo.subjectid, 
                    sectionid: attemptInfo.sectionid
                }
            })
        }


}

module.exports = attemptDb;