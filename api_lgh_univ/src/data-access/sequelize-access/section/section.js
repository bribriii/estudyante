const Section = require('../../models').Section;



const sectionDb = () => {
    return Object.freeze({
        listSections,
        findSection,
        addSection,
        updateSection,
        deactivateSection,
        activateSection
    })

    async function listSections(){
        return Section
            .findAll();
    }

    async function findSection(sectionInfo){
        return Section
            .findAll({
                where: {
                    sectionid: sectionInfo.sectionid,
                    subjectid: sectionInfo.subjectid
                }
            })
    }

    async function addSection(sectionInfo){
        return Section
            .create({
                sectionid: sectionInfo.sectionid,
                subjectid: sectionInfo.subjectid,
                classsize: sectionInfo.classsize,
                active: true
            })
    }

    async function updateSection(sectionInfo){
        return Section
            .update({
                classsize: sectionInfo.classsize
            }, {
                where: {
                    sectionid: sectionInfo.sectionid,
                    subjectid: sectionInfo.subjectid
                }
            });
    }


    async function deactivateSection(sectionInfo){
        return Section
            .update({
                active: false
            }, {
                where: {
                    sectionid: sectionInfo.sectionid,
                    subjectid: sectionInfo.subjectid
                }
            });
    }

    async function activateSection(sectionInfo){
        return Section
            .update({
                active: true
            }, {
                where: {
                    sectionid: sectionInfo.sectionid,
                    subjectid: sectionInfo.subjectid
                }
            });
    }
}

module.exports = sectionDb;