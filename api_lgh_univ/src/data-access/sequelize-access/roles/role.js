const Role = require('../../models').Role;

const rolesDb = () => {

    return Object.freeze({
        add, 
        find, 
        list, 
        update, 
        deactivate
    });

    async function getrolecount(){
        var count = await Role.count();
        return count;
    }
    
    async function add({...roleInfo}){
        return getrolecount().then(count =>{
            return Role
            .create({
                roleid: (count + 1),
                roledesc: roleInfo.roledesc,
                active: true
            })
            .then( (role) => { return role })
            .catch((err) => { return err }); 
        })
    }

    async function list(){
        return Role
            .findAll({
                attributes: ['roleid', 'roledesc']
            })
            .then((role) => {return role})
            .catch((err) => {return err});
    }

    async function find(id){
        return Role
            .findByPk(id)
            .then((role) => {
                if(!role){
                    return {
                        message: 'requested role id not found'
                    }
                }
                return role;
            })
            .catch((err) => {return err});
    }

    async function update({...roleInfo}){
        const id = parseInt(roleInfo.id);
        return Role
            .findByPk(id)
            .then(role => {
                if(!role){
                    return {
                        message: 'not found'
                    };
                }
                return role
                    .update({
                        roledesc: roleInfo.roledesc
                    })
                    .then(() => {return {role}})
                    .catch((err) => {return {err}});
            })
            .catch((err) => {return {status: 404, err}});
            
    }

    async function deactivate({...roleInfo}){
        const id = parseInt(roleInfo.id);
        return Role
        .findByPk(id)
        .then(role => {
            return role
                .update({
                    active: false
                })
                .then(() => {return role})
                .catch((err) => {return {status: 400, err}});
        })
        .catch((err) => {return {status: 404, err}});
    }
}

module.exports = rolesDb


