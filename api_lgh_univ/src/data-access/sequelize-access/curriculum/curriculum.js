const Curriculum = require('../../models').Curriculum;

const curriculumDb = () => {

    return Object.freeze({
        listCurriculum,
        addCurriculum
    });

    async function listCurriculum(){
        return Curriculum
            .findAll()
    }


    async function addCurriculum(curriculumInfo){
        return Curriculum
            .create({
                subjectid: curriculumInfo.subjectid,
                courseid: curriculumInfo.courseid,
                year: curriculumInfo.year,
                semester: curriculumInfo.semester
            })
            .then((curr) => res.status(201).send(curr))
            .catch((err) => res.status(400).send(err));
    }
}

module.exports = curriculumDb;