const Subjects = require('../../models').Subjects;
const Section = require('../../models').Section;


const subjectDb = () => {
    return Object.freeze({
        listSubjects,
        findSubjectSections,
        findSubject,
        addSubject,
        updateSubject,
        deactivateSubject,
        activateSubject
    });

    async function listSubjects(){
        return Subjects
            .findAll()
    }

    async function findSubjectSections(subjectid){
        return Subjects
            .findByPk(subjectid, 
                {include: [{
                    model: Section, as: 'subjectsections'
                }]}
            );
    } 

    async function findSubject(subjectid){
        return Subjects
            .findByPk(subjectid);
    }


    async function addSubject(subjectInfo){
        return Subjects
            .create({
                subjectid: subjectInfo.subjectid,
                subjectdesc: subjectInfo.subjectdesc,
                creditpoints: subjectInfo.creditpoints,
                active: true
            })
    }

    async function updateSubject(subjectInfo){
        return Subjects
            .update({
                subjectid: subjectInfo.subjectid,
                subjectdesc: subjectInfo.subjectdesc,
                creditpoints: subjectInfo.creditpoints
            }, {where:{
                subjectid: subjectInfo.subjectid
            }
        });
    }

    async function deactivateSubject(subjectid){
        return Subjects
            .update({
                active: false
            }, {where:{
                subjectid: subjectid
            }
        });
    }

    async function activateSubject(subjectid){
        return Subjects
            .update({
                active: true
            }, {where:{
                subjectid: subjectid
            }
        });
    }

}

module.exports = subjectDb;