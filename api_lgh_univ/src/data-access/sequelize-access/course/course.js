const Course = require('../../models').Course;
const Curriculum = require('../../models').Curriculum;
const Subjects = require('../../models').Subjects;


const courseDb = () => {
    return Object.freeze({
        getCourses,
        findCourse,
        findCourseWithCurr,
        addCourse,
        updateCourse,
        deactivateCourse,
        activateCourse
    });

    async function getCourses(){
        return Course
            .findAll();
    }

     async function findCourse(courseid){
        return Course
            .findByPk(courseid)
    }

    async function findCourseWithCurr(courseid){
        return Course
            .findByPk(courseid,
                {include: [
                    {
                        model: Curriculum, 
                            as: 'subjects', 
                            attributes:['subjectid', 'year', 'semester'],
                        include: [{model: Subjects, 
                            as: 'subjectinfo', 
                            attributes:['subjectdesc', 'creditpoints']}]
                    },
                ]},
               { order: [
                    ['year', 'ASC'],
                    ['semester', 'ASC'],
                ]},
            );
    }

    async function addCourse(courseInfo){
        return Course
            .create({
                courseid: courseInfo.courseid,
                coursedesc: courseInfo.coursedesc,
                yearcommenced: new Date().getFullYear(),
                active: true
            });
    }

    async function updateCourse(courseInfo){
        return Course
            .update(
                {coursedesc: courseInfo.coursedesc},
                {where:{
                    courseid: courseInfo.courseid
                }
            });
    }

    async function deactivateCourse(courseid){
        return Course
            .update(
                {active: false},
                {where: {
                    courseid: courseid
                }
            });
    }

    async function activateCourse(courseid){
        return Course
            .update(
                {active: true},
                {where: {
                    courseid: courseid
                }
            });
    }

}

module.exports = courseDb;