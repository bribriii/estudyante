const Account = require('../../models').Account;
const Student = require('../../models').Student;
const Course = require('../../models').Course;
const Role = require('../../models').Role;


const accountsDb = () => {
    return Object.freeze({
        getAccounts,
        createAccount,
        generateId,
        findAccount,
        listStudentAccounts,
        updatePassword,
        deactivateAccount,
        activateAccount,
        updateUserRole
    })

    async function getAccounts(){
        return Account
            .findAll({include: [{
                model: Role, as: 'role'
            }]})
            .then((acc) => {return acc})
            .catch((err) => {return err});
    }

    async function listStudentAccounts(){
        return Account
        .findAll({
                where: {roleid: 1},
                include: [{model: Student, as: 'profile',
                    attributes: ['givenname', 'surname', 'dob', 'yearenrolled'],
                    include: [{model: Course, as: 'course',
                        attributes: ['courseid', 'coursedesc', 'yearcommenced']
                    }]
                }]
            })
            .then((acc) => {return acc})
            .catch((err) => {return err});
    }

    async function findAccount(uid){
        return Account
            .findByPk(uid);      
    }

    async function createAccount({...accountInfo}){    
            return Account
            .create({
                uid: accountInfo.uid,
                password: accountInfo.password,
                roleid: accountInfo.roleid,
                active: true
            })
            .then((acc) => { return acc })
            .catch((err) => { return err });
    }

    async function updatePassword({...accountInfo}){
        return Account.update(
                {password: accountInfo.password},
                {where: {uid: accountInfo.uid}
            });
    }

    async function deactivateAccount(uid){
        console.log(uid)
        return Account.update(
            {active: false},
            {where: {uid: uid}
            });
    }

    async function activateAccount(uid){
        return Account.update(
            {active: true},
            {where: {
                uid: uid
            }});
    }

    async function updateUserRole({...accountInfo}){
        return Account.update(
            {roleid: accountInfo.roleid},
            {where: {
                uid: accountInfo.uid
            }});
    }

    async function generateId(){
        var serial = await Account.count();
        var sSer = serial;
        var year = new Date().getFullYear();
        
        while(sSer.length != 4){
            sSer = "0" + sSer;
        }
            
        var id = (year + "-" + sSer);
        return id;
    }
}

module.exports = accountsDb;