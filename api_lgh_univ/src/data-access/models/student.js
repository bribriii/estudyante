'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    sid: {type: DataTypes.STRING, primaryKey: true},
    givenname: DataTypes.STRING,
    surname: DataTypes.STRING,
    dob: DataTypes.DATE,
    yearenrolled: DataTypes.INTEGER,
    yearlevel: DataTypes.INTEGER,
    courseid: DataTypes.STRING
  }, {});
  Student.associate = function(models) {
    // associations can be defined here
    // Student.hasOne(models.Account, {
    //   foreignKey: 'sid',
    //   targetKey: 'sid',
    //   as: 'student'
    // });

    Student.belongsTo(models.Course, {
      foreignKey: 'courseid',
      targetKey: 'courseid',
      as: 'course'
    });

    Student.hasMany(models.Attempt, {
      foreignKey: 'sid',
      targetKey: 'sid',
      as: 'grades'
    });


  };
  return Student;
};