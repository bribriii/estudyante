'use strict';
module.exports = (sequelize, DataTypes) => {
  const Section = sequelize.define('Section', {
    sectionid: {type: DataTypes.STRING, primaryKey: true},
    subjectid: {type: DataTypes.STRING, primaryKey: true},
    classsize: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {});
  Section.associate = function(models) {
    // associations can be defined here
  };
  return Section;
};