'use strict';
module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    courseid:{ type: DataTypes.STRING, primaryKey: true},
    coursedesc: DataTypes.STRING,
    yearcommenced: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {});
  Course.associate = function(models) {
    // associations can be defined here
    Course.hasMany(models.Student, {
      foreignKey: 'courseid',
      targetKey: 'courseid',
      as: 'enrolled_students'
    });

    Course.hasMany(models.Curriculum, {
       foreignKey: 'courseid',
       targetKey: 'courseid',
       as: 'subjects'
    });
  };
  return Course;
};