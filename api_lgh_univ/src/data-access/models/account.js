'use strict';
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    uid: {type: DataTypes.STRING, primaryKey: true},
    roleid: DataTypes.INTEGER,
    password: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {});
  Account.associate = function(models) {
    // associations can be defined here
    Account.belongsTo(models.Role, {
      foreignKey: 'roleid',
      targetKey: 'roleid',
      as: 'role'
    });

    Account.hasOne(models.Student, {
      foreignKey: 'sid',
      targetKey: 'uid',
      as: 'profile'
    })
  };
  return Account;
};