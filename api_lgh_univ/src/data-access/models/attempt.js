'use strict';
module.exports = (sequelize, DataTypes) => {
  const Attempt = sequelize.define('Attempt', {
    sy: {type: DataTypes.STRING, primaryKey: true},
    semester: {type: DataTypes.STRING, primaryKey: true},
    sid: {type: DataTypes.STRING, primaryKey: true},
    subjectid:  {type: DataTypes.STRING, primaryKey: true},
    sectionid:  {type: DataTypes.STRING, primaryKey: true},
    mark: DataTypes.STRING,
    grade: DataTypes.STRING
  }, {});
  Attempt.associate = function(models) {
    // associations can be defined here
    Attempt.belongsTo(models.Student, {
      foreignKey: 'sid',
      targetKey: 'sid',
      as: 'student'
    })
    
  };
  return Attempt;
};