'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    roleid: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true,},
    roledesc: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
  };
  return Role;
};