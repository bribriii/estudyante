'use strict';
module.exports = (sequelize, DataTypes) => {
  const Curriculum = sequelize.define('Curriculum', {
    subjectid:{type: DataTypes.STRING, primaryKey: true},
    courseid: {type: DataTypes.STRING, primaryKey: true},
    year: DataTypes.INTEGER,
    semester: DataTypes.STRING
  }, {});
  Curriculum.associate = function(models) {
    // associations can be defined here
    Curriculum.hasMany(models.Subjects, {
      foreignKey: 'subjectid',
      targetKey: 'subjectid',
      as: 'subjectinfo'
    });
  };
  return Curriculum;
};