'use strict';
module.exports = (sequelize, DataTypes) => {
  const Schoolyear = sequelize.define('Schoolyear', {
    sy: {type: DataTypes.STRING, primaryKey: true},
    semester: {type: DataTypes.STRING, primaryKey: true},
    active: DataTypes.BOOLEAN
  }, {});
  Schoolyear.associate = function(models) {
    // associations can be defined here
  };
  return Schoolyear;
};