'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subjects = sequelize.define('Subjects', {
    subjectid: {type: DataTypes.STRING, primaryKey: true},
    subjectdesc: DataTypes.STRING,
    creditpoints: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {});
  Subjects.associate = function(models) {
    // associations can be defined here
    Subjects.belongsTo(models.Attempt, {
      as: 'attempts',
      foreignKey: 'subjectid',
      targetKey: 'subjectid'
    })

    Subjects.hasMany(models.Section, {
      as: 'subjectsections',
      foreignKey: 'subjectid',
      targetKey: 'subjectid'
    })

    // Subjects.belongsToMany(models.Curriculum, {
    //   foreignKey: 'subjectid',
    //   targetKey: 'subjectid',
    //   as: 'curricculum'
    // });
  };
  return Subjects;
};