const bcrypt = require('bcrypt');


const bcryptFunctions = () => {
    return Object.freeze({
        makeDefaultPass, 
        checkMatch,
        hash
    });
    
    async function makeDefaultPass(){
        return hash("letmein123");
    }

    async function checkMatch(requested, recorded){
        return bcrypt.compare(requested, recorded);
    }

    async function hash(str){
        const hashed = await new Promise((resolve, reject) => {
            bcrypt.hash(str, 5, function (err, hash) {
                if (err) reject(err)
                resolve(hash)
            });
        })
        return hashed;
    }
}

module.exports = bcryptFunctions;


