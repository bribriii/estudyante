process.env.NODE_ENV = 'test';

const Account = require('../../../data-access/models').Account;
const Student = require('../../../data-access/models').Student;
const Course = require('../../../data-access/models').Course;
const Role = require('../../../data-access/models').Role;
const Curriculum = require('../../../data-access/models').Curriculum;
const Attempt = require('../../../data-access/models').Attempt;
const Schoolyear = require('../../../data-access/models').Schoolyear;
const Section = require('../../../data-access/models').Section;
const Subject = require('../../../data-access/models').Subjects;


module.exports.init = async function () {
  await Account.destroy({ truncate : true, cascade: true });
  await Student.destroy({ truncate : true, cascade: true });
  await Course.destroy({ truncate : true, cascade: true });
  await Role.destroy({ restartIdentity: true, truncate : true, cascade: true });
  await Curriculum.destroy({ truncate : true, cascade: true });
  await Attempt.destroy({ truncate : true, cascade: true });
  await Schoolyear.destroy({ truncate : true, cascade: true });
  await Section.destroy({ truncate : true, cascade: true });
  await Subject.destroy({ truncate : true, cascade: true });
};