'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Attempts', {
      sid: {
        type: Sequelize.STRING,
        references: {
          model: 'Students',
          key: 'sid'
        },
        onDelete: 'CASCADE',
        primaryKey: true
      },
      sy: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false
      },
      semester: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      sectioncode: {
        type: Sequelize.STRING,
        primaryKey: true,
        references: {
          model: 'Sections',
          key: 'sectioncode'
        },
        onDelete: 'CASCADE'
      },
      subjectcode: {
        type: Sequelize.STRING,
        primaryKey: true,
        references: {
          model: 'Subjects',
          key: 'subjectcode'
        },
        onDelete: 'CASCADE'
      },
      mark: {
        type: Sequelize.STRING
      },
      grade: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Attempts');
  }
};
