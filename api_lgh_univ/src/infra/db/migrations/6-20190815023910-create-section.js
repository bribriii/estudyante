'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Sections', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      sectioncode: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      subjectcode: {
        type: Sequelize.STRING,
        primaryKey:true,
        references:{
          model: 'Subjects',
          key: 'subjectcode'
        },
      },
      classsize: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    },
    {
      indexes: [
          {
              unique: true,
              fields: ['sectioncode', 'subjectcode']
          }
      ]
  });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Sections');
  }
};