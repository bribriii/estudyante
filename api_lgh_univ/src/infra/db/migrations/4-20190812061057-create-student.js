'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Students', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      sid: {
        type: Sequelize.STRING,
        allowNull: false,
        references: { 
          model: 'Accounts',
          key: 'uid'
        },
        onDelete: 'CASCADE',
        unique: true
      },
      givenname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      surname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      dob: {
        allowNull: false,
        type: Sequelize.DATE
      },
      yearenrolled: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      yearlevel: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      coursecode: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: 'Courses',
          key: 'coursecode'
        },
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Students');
  }
};