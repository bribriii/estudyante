'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Curriculums', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      subjectcode: {
        type: Sequelize.STRING,
        references: {
          model: 'Subjects',  
          key: 'subjectcode'
        },
        allowNull: false
      },
      coursecode: {
        type: Sequelize.STRING,
        references: {
          model: 'Courses',  
          key: 'coursecode'
        },
        allowNull: false,
      },
      year: {
        type: Sequelize.INTEGER
      },
      semester: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    },
    {
      indexes: [
          {
              unique: true,
              fields: ['subjectcode', 'coursecode']
          }
      ]
  });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Curriculums');
  }
};