'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Subjects', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      subjectcode: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING
      },
      subjectdesc: {
        allowNull: false,
        type: Sequelize.STRING
      },
      creditpoints: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      courseid: {
        type: Sequelize.STRING,
        references: {
          model: 'Courses',
          key: 'coursecode'
        },
        onDelete: 'CASCADE'
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Subjects');
  }
};
