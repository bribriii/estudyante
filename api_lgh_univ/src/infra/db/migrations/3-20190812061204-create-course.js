'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Courses', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      coursecode: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true
      },
      coursedesc:{
        allowNull:false,
        type: Sequelize.STRING
      },
      yearcommenced:{
        allowNull: false,
        type: Sequelize.INTEGER
      },
      active:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Courses');
  }
};