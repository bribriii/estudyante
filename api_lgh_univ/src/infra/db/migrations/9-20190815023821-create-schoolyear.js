'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Schoolyears', {
      sy: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      semester: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Schoolyears');
  }
};