'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'Curriculums',
      [
        {
          subjectcode: 'MATH17',
          coursecode: 'BSIT',
          year: 1,
          semester: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          subjectcode: 'PE1',
          coursecode: 'BSIT',
          year: 1,
          semester: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Curriculums', null, {});
  }
};
