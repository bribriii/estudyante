'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'Subjects',
      [
        {
          subjectcode: 'MATH17',
          subjectdesc: 'Algebra and Trigonometry I',
          courseid: 'BSIT',
          creditpoints: 6,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          subjectcode: 'PE1',
          subjectdesc: 'Physical Fitness and Health',
          courseid: 'BSIT',
          creditpoints: 3,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Subjects', null, {});
  }
};
