'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'Sections',
      [
        {
          sectioncode: 'A',
          subjectcode: 'MATH17',
          classsize: 45,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          sectioncode: 'B',
          subjectcode: 'MATH17',
          classsize: 45,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          sectioncode: 'A',
          subjectcode: 'PE1',
          classsize: 45,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          sectioncode: 'B',
          subjectcode: 'PE1',
          classsize: 45,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Sections', null, {});
  }
};
