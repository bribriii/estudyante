'use strict';
const { Roles } = require('../../../data-access/models');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'Roles',
      [
        {
          roledesc: 'Student',
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          roledesc: 'Registrar',
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          roledesc: 'Admin',
          active: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Roles', null, {});
  }
};
