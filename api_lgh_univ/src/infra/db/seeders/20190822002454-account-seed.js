const bcrypt = require('bcrypt');
('use strict');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const hashedPass = await new Promise((resolve, reject) => {
      bcrypt.hash('letmein123', 5, function(err, hash) {
        if (err) reject(err);
        resolve(hash);
      });
    });

    return queryInterface.bulkInsert(
      'Accounts',
      [
        {
          uid: '2015-0448',
          password: hashedPass,
          active: true,
          roleid: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          uid: '2015-0449',
          password: hashedPass,
          active: true,
          roleid: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          uid: '2015-0001',
          password: hashedPass,
          active: true,
          roleid: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          uid: '2015-0000',
          password: hashedPass,
          active: true,
          roleid: 3,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Accounts', null, {});
  }
};
