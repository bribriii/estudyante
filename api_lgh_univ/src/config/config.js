require('dotenv').config();

module.exports = {
  development: {
    username: 'postgres',
    password: 'root',
    database: 'lghuniv',
    host: 'db',
    dialect: 'postgres'
  },
  test: {
    username: 'postgres',
    password: 'root',
    database: 'lghuniv_test',
    host: 'db',
    dialect: 'postgres'
  },
  secretKey: 'labanrajud'
};
