const {makeSection} = require('../../entities/section/app');

const updateSectionUseCase = ({ sectionDb }) => {
    return async function update(sectionInfo){
        const sectionEntity = makeSection(sectionInfo);
        let fetched = await sectionDb.findSection({
            sectionid: sectionEntity.getSectionId(),
            subjectid: sectionEntity.getSubjectId()
        });

        if(!fetched){
            throw new Error('section does not exist');
        }

        await sectionDb.updateSection({
            sectionid: sectionEntity.getSectionId(),
            subjectid: sectionEntity.getSubjectId(),
            classsize: sectionEntity.getClassSize()
        });

        fetched = await sectionDb.findSection({
            sectionid: sectionEntity.getSectionId(),
            subjectid: sectionEntity.getSubjectId()
        });

        return fetched;
    }
}

module.exports = updateSectionUseCase;