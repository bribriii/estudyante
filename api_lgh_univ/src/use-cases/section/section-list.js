const listSectionsUseCase = ({ sectionDb }) => {
    return async function list(){
        return sectionDb.listSections();
    }
}

module.exports = listSectionsUseCase;