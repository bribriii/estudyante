const activateSectionUseCase = ({ sectionDb }) => {
    return async function activate(sectionInfo){
        let fetched = await sectionDb.findSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        if(!fetched){
            throw new Error('section does not exist');
        }
        if(fetched.active){
            throw new Error('section already active');
        }

        await sectionDb.activateSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        fetched = await sectionDb.findSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        return fetched;
    }
}

module.exports = activateSectionUseCase;