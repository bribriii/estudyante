const deactivateSectionUseCase = ({ sectionDb }) => {
    return async function deactivate(sectionInfo){
        let fetched = await sectionDb.findSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        if(!fetched){
            throw new Error('section does not exist');
        }
        if(!fetched.active){
            throw new Error('section already deactivated');
        }

        await sectionDb.activateSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        fetched = await sectionDb.findSection({
            sectionid: sectionInfo.sectionid,
            subjectid: sectionInfo.subjectid
        });

        return fetched;
    }
}

module.exports = deactivateSectionUseCase;