const sectionDb = require('../../data-access/sequelize-access/section/app');
const subjectDb = require('../../data-access/sequelize-access/subjects/app');


const createSectionUseCase = require('./section-create');
const findSectionUseCase = require('./section-find');
const listSectionsUseCase = require('./section-list');
const updateSectionUseCase = require('./section-update');
const activateSectionUseCase = require('./section-activate');
const deactivateSectionUseCase = require('./section-deactivate');

const createSection = createSectionUseCase({ sectionDb, subjectDb });
const findSection = findSectionUseCase({ sectionDb });
const listSection = listSectionsUseCase({ sectionDb });
const updateSection = updateSectionUseCase({ sectionDb });
const activateSection = activateSectionUseCase({ sectionDb });
const deactivateSection = deactivateSectionUseCase({ sectionDb });


const sectionServices = {
    createSection,
    findSection,
    listSection,
    updateSection,
    activateSection,
    deactivateSection
}


module.exports = sectionServices;
module.exports = {
    createSection,
    findSection,
    listSection,
    updateSection,
    activateSection,
    deactivateSection
}