const findSectionsUseCase = ({ sectionDb }) => {
    return async function list(sectionInfo){
        return sectionDb.findSection(sectionInfo);
    }
}

module.exports = findSectionsUseCase;
