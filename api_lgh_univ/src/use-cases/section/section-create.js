
const { makeSection } = require('../../entities/section/app');

const createSectionUseCase = ({ sectionDb, subjectDb }) => {
    return async function add(sectionInfo){
        const sectionEntity = makeSection(sectionInfo);
        
        let fetchSubj = await subjectDb.findSubject(sectionEntity.getSubjectId());
        if(!fetchSubj){
            throw new Error('subject does not exist');
        }

        const addedSection = await sectionDb.addSection({
            sectionid: sectionEntity.getSectionId(),
            subjectid: sectionEntity.getSubjectId(),
            classsize: sectionEntity.getClassSize()
        });

        if(!addedSection){
            throw new Error('can\'t add section');
        }

        return addedSection;

    }
}

module.exports = createSectionUseCase;