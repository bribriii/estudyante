const useCaseDeactivateCourse = ({ courseDb }) => {
    return async function activateCourse(courseid){
        let fetch = await courseDb.findCourse(courseid);
        if(!fetch){
            throw new Error('requested course not found');
        };
        if(!fetch.active){
            throw new Error('course already deactivated');
        }

        await courseDb.deactivateCourse(courseid);
        fetch = await courseDb.findCourse(courseid);

        return fetch;
    }
}

module.exports = useCaseDeactivateCourse;