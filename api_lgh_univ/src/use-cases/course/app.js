const courseDb = require('../../data-access/sequelize-access/course/app');

const createCourseUseCase = require('./course-create');
const getCoursesUseCase = require('./course-getAll');
const findCourseUseCase = require('./course-find');
const updateCourseUseCase = require('./course-update');
const activateCourseUseCase = require('./course-activate');
const deactivateCourseUseCase = require('./course-deactivate');

const createCourse = createCourseUseCase({ courseDb });
const getCourses = getCoursesUseCase({ courseDb });
const findCourse = findCourseUseCase({ courseDb });
const updateCourse = updateCourseUseCase({ courseDb });
const activateCourse = activateCourseUseCase({ courseDb });
const deactivateCourse = deactivateCourseUseCase({ courseDb })


const courseService = Object.freeze({
    createCourse,
    getCourses,
    findCourse,
    updateCourse,
    activateCourse,
    deactivateCourse
});

module.exports = courseService;
module.exports = {
    createCourse,
    getCourses,
    findCourse,
    updateCourse,
    activateCourse,
    deactivateCourse
};