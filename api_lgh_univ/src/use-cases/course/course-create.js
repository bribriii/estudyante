const { makeCourse } = require('../../entities/course/app');

const createCourseUseCase = ({ courseDb }) => {
    return async function createCourse(courseInfo){

        const courseEntity = makeCourse(courseInfo);

        return courseDb.addCourse({
            courseid: courseEntity.getCourseId(),
            coursedesc: courseEntity.getCourseDesc(),
            yearcommenced: courseEntity.getYearCommenced()
        });
    }
}

module.exports = createCourseUseCase;