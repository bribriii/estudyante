const findCourseUseCase = ({ courseDb }) => {
    return async function findCourse(courseid){
        const course =  await courseDb.findCourseWithCurr(courseid);
        if(!course){
            throw new Error("no records found");
        }
        return course
    }
}

module.exports = findCourseUseCase;