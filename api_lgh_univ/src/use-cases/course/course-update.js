const {makeCourseUpdate} = require('../../entities/course/app');

const useCaseUpdateCourse = ({ courseDb }) => {
    return async function updateCourse(courseInfo){
        const courseUpdateEntity = await makeCourseUpdate(courseInfo);

        let fetch = await courseDb.findCourse(courseUpdateEntity.getCourseId());

        if(!fetch){
            throw new Error('requested course not found');
        }

        await courseDb.updateCourse({
            courseid: courseUpdateEntity.getCourseId(),
            coursedesc: courseUpdateEntity.getCourseDesc()
        });
        fetch = await courseDb.findCourse(courseUpdateEntity.getCourseId());

        return fetch;
    }
}

module.exports = useCaseUpdateCourse;