const useCaseGetCourses = ({ courseDb }) => {
    return async function getCourses(){
        return courseDb.getCourses();
    }
}

module.exports = useCaseGetCourses;