const { makeRole } = require("../../entities/role/app");

const makeRoles = ({ roleDb }) => {
  return async function useCaseRoleInsert(roleInfo) {
    const info = makeRole(roleInfo);
    const exists = await roleDb.find(info.getId());
    if (exists.rowCount !== 0) {
      return exists;
    }

    return roleDb.add({
      roledesc: info.getRoleDesc()
    });
  };
};

module.exports = makeRoles;