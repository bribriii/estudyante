const deactivateRole = ({roleDb}) => {
    return async function deacRole({id} = {}){

        const dRole = await roleDb.find(id);
        if(dRole.rowCount === 0){
            return deactivateNothing();
        }
         const deactivated = await roleDb.deactivate({id});
         return {
            msg: "role deactivated",
            dRole,
            deactivated
        };
    }
    function deactivateNothing(){
        return {
            deletedCount: 0,
            msg: "role not found"
          };
    }
}

module.exports = deactivateRole;
