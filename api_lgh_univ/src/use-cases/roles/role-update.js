//get entity for attribute validation
const {makeRoleUpdate} = require("../../entities/role/app");

const updateRoles = ({ roleDb }) => {
    return async function useCaseRoleUpdate({id, ...roleInfo}){
        
        const role = makeRoleUpdate(roleInfo);
        const updated = await roleDb.update({
            id: id,
            roledesc: role.getRoleDesc()
        });

        return {...updated}
    }
}

module.exports = updateRoles;