const getRoles = ({ roleDb }) => {
    return async function useCaseGetRoles(){
        return roleDb.list();
    }
}

module.exports = getRoles;