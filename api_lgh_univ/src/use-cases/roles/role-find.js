const findRole = ({roleDb}) => {
    return async function useCaseFindRole(id){
        const role = await roleDb.find(id);
        return role;
    }
}

module.exports = findRole