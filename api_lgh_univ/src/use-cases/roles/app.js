//access the role data-access layer
const roleDb = require("../../data-access/sequelize-access/roles/app");


const createRoles = require("./role-insert");
const listRoles = require("./role-getall");
const updateRoles = require("./role-update");
const deacRoles = require("./role-deactivate");
const findRoles = require("./role-find");


//pass roleDb to use cases
const insertRole = createRoles({roleDb});
const listRole = listRoles({roleDb});
const updateRole = updateRoles({roleDb});
const deacRole = deacRoles({roleDb});
const findRole = findRoles({roleDb});


const roleService = Object.freeze({
    insertRole,
    listRole,
    updateRole,
    deacRole,
    findRole
});

module.exports = roleService;
module.exports = {
    insertRole,
    listRole,
    updateRole,
    deacRole,
    findRole
}