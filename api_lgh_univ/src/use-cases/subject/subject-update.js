const { makeSubjectUpdate } = require('../../entities/subject/app');

const useCaseUpdateSubject = ({ subjectDb }) => {
    return async function update(subjectInfo){
       
        const subjectUpdateEntity = await makeSubjectUpdate(subjectInfo);

        let fetch = await subjectDb.findSubject(subjectUpdateEntity.getSubjectId());

        if(!fetch){
            throw new Error('requested subject id does not exits');
        }

        await subjectDb.updateSubject({
            subjectid: subjectUpdateEntity.getSubjectId(),
            subjectdesc: subjectUpdateEntity.getCourseDesc(),
            creditpoints: subjectUpdateEntity.getCreditPoints()
        });
        
        fetch = await subjectDb.findSubject(subjectUpdateEntity.getSubjectId());

        return fetch;
    }
}

module.exports = useCaseUpdateSubject;