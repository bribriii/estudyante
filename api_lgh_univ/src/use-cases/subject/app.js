const subjectDb = require('../../data-access/sequelize-access/subjects/app');


const createSubjectUseCase = require('./subject-add');
const findSubjectUseCase = require('./subject-find');
const findSubjectSectionsUseCase = require('./subject-find-sections');
const listSubjectsUseCase = require('./subject-list');
const deactivateSubjectUseCase = require('./subject-deactivate');
const activateSubjectUseCase = require('./subject-activate');
const updateSubjectUseCase = require('./subject-update');

const createSubject = createSubjectUseCase({ subjectDb });
const findSubject = findSubjectUseCase({ subjectDb });
const findSubjectSections = findSubjectSectionsUseCase({ subjectDb });
const listSubjects = listSubjectsUseCase({ subjectDb });
const deactivateSubject = deactivateSubjectUseCase({ subjectDb });
const activateSubject = activateSubjectUseCase({ subjectDb });
const updateSubject = updateSubjectUseCase({ subjectDb });

const subjectServices = Object.freeze({
    createSubject,
    findSubject,
    findSubjectSections,
    listSubjects,
    deactivateSubject,
    activateSubject,
    updateSubject
});

module.exports = subjectServices;
module.exports = {
    createSubject,
    findSubject,
    findSubjectSections,
    listSubjects,
    deactivateSubject,
    activateSubject,
    updateSubject
}