const useCaseDeactivateSubject = ({ subjectDb }) => {
    return async function deactivate(subjectid){
        let fetch = await subjectDb.findSubject(subjectid);
        if(!fetch){
            throw new Error('requested course not found');
        };
        if(!fetch.active){
            throw new Error('course already deactivated');
        }

        await subjectDb.deactivateSubject(subjectid)
        fetch = await subjectDb.findSubject(subjectid);

        return fetch;
    }
}

module.exports = useCaseDeactivateSubject;