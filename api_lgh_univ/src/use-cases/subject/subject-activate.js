const useCaseActivateSubject = ({ subjectDb }) => {
    return async function activate(subjectid){
        let fetch = await subjectDb.findSubject(subjectid);
        if(!fetch){
            throw new Error('requested course not found');
        };
        if(fetch.active){
            throw new Error('course already active');
        }

        await subjectDb.activateSubject(subjectid)
        fetch = await subjectDb.findSubject(subjectid);

        return fetch;
    }
}

module.exports = useCaseActivateSubject;