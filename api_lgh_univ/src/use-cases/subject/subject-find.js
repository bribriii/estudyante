const useCaseFindSubject = ({ subjectDb }) => {
    return async function fetch(subjectid){
        const fetched = await subjectDb.findSubject(subjectid);

        if(!fetched){
            throw new Error('requested subject not found');
        }

        return fetched;
    }
}

module.exports = useCaseFindSubject;