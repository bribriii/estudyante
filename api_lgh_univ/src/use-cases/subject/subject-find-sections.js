const useCaseFindSubjectSection = ({ subjectDb }) => {
    return async function find(subjectid){
        const fetch = await subjectDb.findSubjectSections(subjectid);
        if(!fetch){
            throw new Error('requested subject not found');
        }

        return fetch;
    }
}

module.exports = useCaseFindSubjectSection;