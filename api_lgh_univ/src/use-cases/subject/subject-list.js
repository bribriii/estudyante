const useCaseListSubjects = ({ subjectDb }) => {
    return async function list(){
        return subjectDb.listSubjects();
    }
}

module.exports = useCaseListSubjects;