const {makeSubject} = require('../../entities/subject/app');

const useCaseAddSubject = ({ subjectDb }) => {
    return async function add(subjectinfo){
        const subjectEntity = await makeSubject(subjectinfo);

        const fetch = await subjectDb.findSubject(subjectEntity.getSubjectId());

        if(fetch){
            throw new Error('subject id already exists');
        }

        return subjectDb.addSubject({
            subjectid: subjectEntity.getSubjectId(),
            subjectdesc: subjectEntity.getSubjectDesc(),
            creditpoints: subjectEntity.getCreditPoints()
        })
    }
}

module.exports = useCaseAddSubject;