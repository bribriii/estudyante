const { makeSy } = require('../../entities/schoolyear/app');

const createSyUseCase = ({ schoolyearDb }) => {
    return async function create(syInfo){

        const syEntity = makeSy(syInfo);

        return schoolyearDb.addSy({
            sy: syEntity.getSy(),
            semester: syEntity.getSemester()
        });
    }
}

module.exports = createSyUseCase;