const schoolyearDb = require('../../data-access/sequelize-access/schoolyear/app');

const createSyUseCase = require('./sy-add');
const listSyUseCase = require('./sy-list');
const findSyUseCase = require('./sy-find');

const createSy = createSyUseCase({ schoolyearDb });
const listSy = listSyUseCase({ schoolyearDb });
const findSy = findSyUseCase({ schoolyearDb });

const syService = Object.freeze({
    createSy,
    listSy,
    findSy
});

module.exports = syService;
module.exports = {
    createSy,
    listSy,
    findSy
}