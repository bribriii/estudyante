const findSyUseCase = ({ schoolyearDb }) => {
    return async function findCourse(syInfo){
        const sy =  await schoolyearDb.findSy(syInfo);
        if(!sy){
            throw new Error("no records found");
        }
        return sy
    }
}

module.exports = findSyUseCase;