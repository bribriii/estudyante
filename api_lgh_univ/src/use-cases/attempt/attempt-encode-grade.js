const { makeAttemptGrade } = require('../../entities/attempt/app');

const createAttemptGradeUseCase = ({ attemptDb }) => {
    return async function createGrade(attemptInfo){
        let fetch =  await attemptDb.findAttempt({
            sid: attemptInfo.sid,
            semester: attemptInfo.semester, 
            sy: attemptInfo.sy, 
            subjectid: attemptInfo.subjectid, 
            sectionid: attemptInfo.sectionid
        });

        if(!fetch){
            throw new Error("no records found");
        }

        const attemptEntity = makeAttemptGrade(attemptInfo);

        const encoded = attemptDb.encodeAttemptGrade({
            sid: attemptEntity.getSid(),
            sy: attemptEntity.getSy(),
            semester: attemptEntity.getSemester(),
            sectionid: attemptEntity.getSectiondId(),
            subjectid: attemptEntity.getSubjectId(),
            mark: attemptEntity.getMark(),
            grade: attemptEntity.getGrade()
        });

        fetch =  await attemptDb.findAttempt({
            sid: attemptInfo.sid,
            semester: attemptInfo.semester, 
            sy: attemptInfo.sy, 
            subjectid: attemptInfo.subjectid, 
            sectionid: attemptInfo.sectionid
        });

        return fetch;
    }
}

module.exports = createAttemptGradeUseCase;