const { makeAttempt } = require('../../entities/attempt/app');

const createAttemptUseCase = ({ attemptDb, schoolyearDb }) => {
    return async function create(attemptInfo){
        const current = await schoolyearDb.getCurrentSy();
        const _attemptInfo = {
            sy: current.sy, 
            semester: current.semester, 
            ...attemptInfo
        };

        const attemptEntity = makeAttempt(_attemptInfo);
        const created = attemptDb.createNewAttempt({
            sid: attemptEntity.getSid(),
            sy: attemptEntity.getSy(),
            semester: attemptEntity.getSemester(),
            sectionid: attemptEntity.getSectiondId(),
            subjectid: attemptEntity.getSubjectId()
        });

        return created;
    }
}

module.exports = createAttemptUseCase;