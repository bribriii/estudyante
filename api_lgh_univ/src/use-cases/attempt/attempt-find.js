const findAttemptUseCase = ({ attemptDb }) => {
    return async function findCourse(attemptInfo){
        const fetch =  await attemptDb.findAttempt({
            sid: attemptInfo.sid,
            semester: attemptInfo.semester, 
            sy: attemptInfo.sy, 
            subjectid: attemptInfo.subjectid, 
            sectionid: attemptInfosectionid
        });
        if(!fetch){
            throw new Error("no records found");
        }
        return fetch
    }
}

module.exports = findAttemptUseCase;