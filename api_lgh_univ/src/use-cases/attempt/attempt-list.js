const listAttemptsUseCase = ({ attemptDb }) => {
    return async function list(){
        return attemptDb.listAttempts();
    }
}

module.exports = listAttemptsUseCase;