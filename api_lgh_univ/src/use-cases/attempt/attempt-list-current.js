const listCurrentAttemptsUseCase = ({ attemptDb, schoolyearDb }) => {
    return async function listCurrent(){
        const currentSy = await schoolyearDb.getCurrentSy();

        return attemptDb.listAttempts({
            sy: currentSy.sy,
            semester: currentSy.semester
        });
    }
}

module.exports = listCurrentAttemptsUseCase;