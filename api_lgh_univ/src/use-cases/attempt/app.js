const attemptDb = require('../../data-access/sequelize-access/attempt/app');
const schoolyearDb = require('../../data-access/sequelize-access/schoolyear/app');

const createNewAttemptUseCase = require('./attempt-create');
const createAttemptGradeUseCase = require('./attempt-encode-grade');
const findAttemptUseCase = require('./attempt-find');
const listAttemptsUseCase = require('./attempt-list');
const listCurrentAttemptsUseCase = require('./attempt-list-current');

const createNewAttempt = createNewAttemptUseCase({ attemptDb, schoolyearDb });
const createAttemptGrade = createAttemptGradeUseCase({ attemptDb });
const findAttempt = findAttemptUseCase({ attemptDb });
const listAttempts = listAttemptsUseCase({ attemptDb });
const listCurrentAttempts = listCurrentAttemptsUseCase({ attemptDb, schoolyearDb });


const attemptService = {
    createNewAttempt,
    createAttemptGrade,
    findAttempt,
    listAttempts,
    listCurrentAttempts
}

module.exports = attemptService;
module.exports = {
    createNewAttempt,
    createAttemptGrade,
    findAttempt,
    listAttempts,
    listCurrentAttempts
}