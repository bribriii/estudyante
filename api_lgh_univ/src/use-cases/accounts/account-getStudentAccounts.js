const getStudentAccounts = ({ accountDb }) => {
    return async function useCaseGetStudentAccounts(){
        const studentAccounts = await accountDb.listStudentAccounts();
        return studentAccounts;
    }
}

module.exports = getStudentAccounts