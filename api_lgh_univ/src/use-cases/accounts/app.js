const accountDb = require("../../data-access/sequelize-access/account/app");

const getAccountsUseCase = require("./account-getall");
const createAccountUseCase = require("./account-create");
const findAccountUseCase = require("./account-find");
const getStudentAccountsUseCase = require("./account-getStudentAccounts");
const authenticateUseCase = require("./account-authenticate");
const updatePassUseCase = require('./account-updatePassword');
const resetPassUseCase = require('./account-resetPassword');
const activateAccountUseCase = require('./account-activate');
const deactivateAccountUseCase = require('./account-deactivate');
const updateRoleUseCase = require('./account-updateAcctRole');

const listAccounts = getAccountsUseCase({accountDb});
const createAccount = createAccountUseCase({ accountDb});
const findAccount = findAccountUseCase({ accountDb });
const getStudentAccounts = getStudentAccountsUseCase({ accountDb });
const authenticate = authenticateUseCase({ accountDb });
const updatePassword = updatePassUseCase({ accountDb });
const resetPassword = resetPassUseCase({ accountDb });
const activateAccount = activateAccountUseCase({ accountDb });
const deactivateAccount = deactivateAccountUseCase({ accountDb });
const updateUserRole = updateRoleUseCase({ accountDb });

const accountService = Object.freeze({
    listAccounts,
    createAccount,
    findAccount,
    getStudentAccounts,
    authenticate,
    updatePassword,
    resetPassword,
    activateAccount,
    deactivateAccount,
    updateUserRole
});

module.exports = accountService;
module.exports = {
    listAccounts,
    createAccount,
    findAccount,
    getStudentAccounts,
    authenticate,
    updatePassword,
    resetPassword,
    activateAccount,
    deactivateAccount,
    updateUserRole
}
