const useCaseActivateAccount = ({ accountDb }) => {
    return async function activateUserAccount(uid){
        let fetchAcct = await accountDb.findAccount(uid);

        if(!fetchAcct){
            throw new Error('requested account not found');
        }
        if(fetchAcct.active){
            throw new Error('account is already active');
        }

        await accountDb.activateAccount(uid.trim());

        fetchAcct = await accountDb.findAccount(uid);

        return fetchAcct;
    }
}

module.exports = useCaseActivateAccount;