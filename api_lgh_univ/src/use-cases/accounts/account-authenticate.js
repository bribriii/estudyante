const { makeAuth } = require("../../entities/account/app");
const checkMatch = require("../../infra/bcrypt/app");

const useCaseAuthenticate = ({ accountDb }) => {
    return async function authenticateAccount(authDetails){
        const auth = makeAuth(authDetails);

        const fetchedAcct = await accountDb.findAccount(auth.getUid());
        if(!fetchedAcct){
            throw new Error('requested id not found');
        }

        const matched = await checkMatch.checkMatch(auth.getPassword(), fetchedAcct.password);
        console.log(matched);
        if(!matched){
            throw new Error('incorrect password');
        }
        
        return fetchedAcct;
    }
}

module.exports = useCaseAuthenticate