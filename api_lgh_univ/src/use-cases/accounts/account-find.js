const findAccount = ({accountDb}) => {
    return async function useCaseFindAccount(requestedId){
        const acct = await accountDb.findAccount(requestedId.uid);
        if(!acct){
            throw new Error('requested uid not found!');
        }
        return acct;
    }
}

module.exports = findAccount