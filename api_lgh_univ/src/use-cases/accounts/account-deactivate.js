const useCaseDeactivateAccount = ({ accountDb }) => {
    return async function deactivateUserAccount(uid){
        let fetchAcct = await accountDb.findAccount(uid);

        if(!fetchAcct){
            throw new Error('requested account not found');
        }
        if(!fetchAcct.active){
            throw new Error('account is already inactive');
        }

        await accountDb.deactivateAccount(uid.trim());
        fetchAcct = await accountDb.findAccount(uid);

        return fetchAcct;
    }
}

module.exports = useCaseDeactivateAccount;