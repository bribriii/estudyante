const { makeAccount } = require("../../entities/account/app");

const createAccount = ({accountDb}) => {
    return async function useCaseCreateAccount(accountInfo){
        const info = makeAccount(accountInfo);

        return accountDb.createAccount({
            uid: info.getUid(),
            password: info.getPassword(),
            roleid: info.getRoleId()            
        });
    }
}

module.exports = createAccount;