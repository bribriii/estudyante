const { makeAuth } = require('../../entities/account/app');

const useCaseUpdatePassword = ({ accountDb }) => {
    return async function updateUserPassword(authDetails){
        const auth = makeAuth(authDetails);

        let fetchAcct = await accountDb.findAccount(auth.getUid());

        if(!fetchAcct){
            throw new Error('requested account not found');
        }

        await accountDb.updatePassword({
            uid: auth.getUid(),
            password: auth.getPassword()
        })

        fetchAcct = await accountDb.findAccount(auth.getUid());

        return fetchAcct;
    }
}

module.exports = useCaseUpdatePassword;
