const getAccounts = ({ accountDb }) => {
    return async function useCaseGetAccounts(){
        return accountDb.getAccounts();
    }
}

module.exports = getAccounts