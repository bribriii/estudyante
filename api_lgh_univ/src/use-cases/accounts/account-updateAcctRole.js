const { makeAcctRoleUpdate } = require('../../entities/account/app');

const useCaseUpdateRole = ({ accountDb }) => {
    return async function updateUserRole(acctInfo){
        const acct = await makeAcctRoleUpdate(acctInfo)

        let fetchAcct = await accountDb.findAccount(acct.getUid());

        if(!fetchAcct){
            throw new Error('requested account not found');
        }

        await accountDb.updateUserRole({
            roleid: acct.getRoleId(),
            uid: acct.getUid()
        });

        fetchAcct = await accountDb.findAccount(acct.getUid());

        return fetchAcct;
    }
}

module.exports = useCaseUpdateRole;