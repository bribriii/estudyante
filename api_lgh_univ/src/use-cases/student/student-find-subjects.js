const findStudentSubjectUseCase = ({ studentDb }) => {
    return async function findCourse(sid){
        const student =  await studentDb.findStudentSubjects(sid);
        if(!student){
            throw new Error("no records found");
        }
        return student;
    }
}

module.exports = findStudentSubjectUseCase;