const { makeStudent } = require('../../entities/student/app');
const { makeAccount } = require('../../entities/account/app');

const createStudentUseCase = ({ studentDb, accountDb }) => {
    return async function addStudent(studentInfo){

        const info = makeAccount(studentInfo);
        const student = makeStudent({...studentInfo});

        const newStudentAccount = await accountDb.createAccount({
            uid: info.getUid(),
            password: info.getPassword(),
            roleid: info.getRoleId()            
        });


        if(!newStudentAccount){
            throw new Error('can\'t add new student account');
        }

        const newStudentProfile = await studentDb.createStudent({
            sid: info.getUid(),
            givenname: student.getGivenName(),
            surname: student.getSurname(),
            yearenrolled: student.getYearEnrolled(),
            yearlevel: student.getYearLevel(),
            dob: student.getDob(),
            courseid: student.getCourse()
        });

        if(!newStudentProfile){
            throw new Error('can\'t add new student profile');
        }

        return {newStudentAccount, newStudentProfile}
    }
}

module.exports = createStudentUseCase