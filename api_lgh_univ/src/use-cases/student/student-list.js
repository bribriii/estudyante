const useCaseListStudents = ({ studentDb }) => {
    return async function listStudents(){
        return studentDb.listStudents();
    }
}

module.exports = useCaseListStudents;