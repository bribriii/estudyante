const findStudentGradesUseCase = ({ studentDb }) => {
    return async function findCourse(sid){
        const student =  await studentDb.findStudentGrades(sid);
        if(!student){
            throw new Error("no records found");
        }
        return student;
    }
}

module.exports = findStudentGradesUseCase;