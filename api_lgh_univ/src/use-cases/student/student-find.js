const findStudentUseCase = ({ studentDb }) => {
    return async function findCourse(sid){
        const student =  await studentDb.findStudent(sid);
        if(!student){
            throw new Error("no records found");
        }
        return student;
    }
}

module.exports = findStudentUseCase;