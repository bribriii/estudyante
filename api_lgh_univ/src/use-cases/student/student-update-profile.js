const { makeStudentUpdate } = require('../../entities/student/app');

const useCaseUpdateStudent = ({ studentDb }) => {
    return async function updateStudent(studentInfo){
        const studentUpdateEntity = await makeStudentUpdate(studentInfo);

        let fetch = await studentDb.findStudent(studentUpdateEntity.getSid());

        if(!fetch){
            throw new Error('requested student id not found');
        }

        await studentDb.updateStudentProfile({
            givenname: studentUpdateEntity.getGivenName(),
            surname: studentUpdateEntity.getSurname(),
            yearlevel: studentUpdateEntity.getYearLevel(),
            dob: studentUpdateEntity.getDob(),
            courseid: studentUpdateEntity.getCourse(),
            sid: studentUpdateEntity.getSid()
        });

        fetch = await studentDb.findStudent(studentUpdateEntity.getSid());
        return fetch;

    }
}

module.exports = useCaseUpdateStudent