const studentDb = require('../../data-access/sequelize-access/student/app');
const accountDb = require('../../data-access/sequelize-access/account/app');


const createStudentUseCase = require('./student-create');
const getStudentsUseCase = require('./student-list');
const findStudentUseCase = require('./student-find');
const findStudentSubjectUseCase = require('./student-find-subjects');
const findStudentGradesUseCase = require('./student-find-grades');
const updateStudentProfileUseCase = require('./student-update-profile');

const createStudent = createStudentUseCase({ studentDb, accountDb });
const getStudents = getStudentsUseCase({ studentDb });
const findStudent = findStudentUseCase({ studentDb });
const findStudentSubject = findStudentSubjectUseCase({ studentDb });
const findStudentGrades = findStudentGradesUseCase({ studentDb });
const updateStudentProfile = updateStudentProfileUseCase({ studentDb });

const studentServices = Object.freeze({
    createStudent,
    getStudents,
    findStudent,
    findStudentGrades,
    findStudentSubject,
    updateStudentProfile
});

module.exports = studentServices;
module.exports = {
    createStudent,
    getStudents,
    findStudent,
    findStudentGrades,
    findStudentSubject,
    updateStudentProfile
}