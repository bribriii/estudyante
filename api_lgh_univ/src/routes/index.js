var express = require('express');
var dotenv = require('dotenv');
var router = express.Router();

//constants
const middleware = require('../middlewares/middleware');
const makeExpressCallback = require("../express-callback/app");

const {
  createRole,
  listRoles,
  findRoles,
  updateRoles,
  deacRoles,
} = require("../controllers/roles/app");

const {
  listAccount,
  listStudentAccounts,
  createNewAccount,
  fetchAccount,
  authenticateAccount,
  updateAccountPassword,
  resetAccountPassword,
  activateUserAccount,
  deactivateUserAccount,
  updateRole
} = require("../controllers/account/app");

const {
  createNewCourse,
  listCourses,
  getCourse,
  updateCourseInfo,
  activateCourses,
  deactivateCourses
} = require("../controllers/course/app");

const {
  newStudent,
  listStudents,
  getStudent,
  getStudentGrades,
  getStudentSubjects,
  updateStudent
} = require('../controllers/student/app');

const {
  addSubject,
  getSubject,
  getSubjectSections,
  getSubjects,
  deactivateSubj,
  activateSubj,
  updateSubj
} = require('../controllers/subject/app');

const {
  createNewSy,
  getAllSy,
  getSy
} = require('../controllers/schoolyear/app');

const {
  createAttempt,
  getCurrentAttempts,
  encodeAttemptGrade,
  getAttempts,
  fetchAttempt
} = require('../controllers/attempt/app');

const {
  createNewSection,
  sectionActivate,
  sectionDeactivate,
  sectionUpdate,
  getSections,
  fetchSection
} = require('../controllers/section/app');


//============== ROUTES

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({
    message: "hello world"
  })
});

//login
router.post('/login', makeExpressCallback(authenticateAccount));

//roles
router.post('/api/roles/insert', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(createRole));
router.get('/api/roles/list', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(listRoles));
router.get('/api/roles/find/:id', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(findRoles));
router.put('/api/roles/:id', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(updateRoles));
router.delete('/api/roles/:id', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(deacRoles));

//accounts
router.get('/api/accounts/list',  middleware.verifytoken, middleware.isAdmin, makeExpressCallback(listAccount));
router.get('/api/accounts/liststudents',middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(listStudentAccounts));
router.post('/api/accounts/newaccount', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(createNewAccount));
router.get('/api/accounts/:uid', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(fetchAccount));
router.post('/api/accounts/updatepass', middleware.verifytoken, makeExpressCallback(updateAccountPassword));
router.post('/api/accounts/resetpass',  middleware.verifytoken, makeExpressCallback(resetAccountPassword));
router.post('/api/accounts/deactivate', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(deactivateUserAccount));
router.post('/api/accounts/activate', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(activateUserAccount));
router.post('/api/accounts/updaterole', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(updateRole));

//courses
router.post('/api/courses/add', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(createNewCourse));
router.get('/api/courses/list',middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(listCourses));
router.get('/api/courses/:courseid', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getCourse));
router.get('/api/courses/curriculum/:courseid',  middleware.verifytoken, makeExpressCallback(getCourse));
router.put('/api/courses/:courseid', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(updateCourseInfo));
router.delete('/api/courses/:courseid', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(deactivateCourses));
router.post('/api/courses/activate/', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(activateCourses))

//student
router.post('/api/students/add', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(newStudent));
router.get('/api/students/list', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(listStudents));
router.get('/api/students/:sid', middleware.verifytoken, makeExpressCallback(getStudent));
router.get('/api/student/subjects/:sid', middleware.verifytoken, makeExpressCallback(getStudentSubjects));
router.post('/api/students/grade/', middleware.verifytoken, makeExpressCallback(getStudentGrades));
router.put('/api/students/:sid',middleware.verifytoken,  middleware.isNotStudent, makeExpressCallback(updateStudent));

//subjects
router.post('/api/subjects/add', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(addSubject));
router.get('/api/subjects/list', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getSubjects));
router.get('/api/subjects/sections/:subjectid', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getSubjectSections));
router.get('/api/subjects/:subjectid', middleware.verifytoken, makeExpressCallback(getSubject));
router.put('/api/subjects/:subjectid', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(updateSubj));
router.delete('/api/subjects/:subjectid', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(deactivateSubj));
router.post('/api/subjects/activate', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(activateSubj));

//schoolyears
router.get('/api/sy/list', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(getAllSy));
router.post('/api/sy/find', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(getSy));
router.post('/api/sy/add', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(createNewSy));

//attempts
router.post('/api/attempts/enroll',  middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(createAttempt));
router.get('/api/attempts/list',  middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getAttempts));
router.get('/api/attempts/listcurrent', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getCurrentAttempts));
router.post('/api/attempts/encodegrade', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(encodeAttemptGrade));
router.post('/api/attempts/', middleware.verifytoken, makeExpressCallback(fetchAttempt));

//section
router.get('/api/sections/list', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(getSections));
router.post('/api/sections/add',  middleware.verifytoken, middleware.isAdmin, makeExpressCallback(createNewSection));
router.post('/api/sections/deactivate', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(sectionDeactivate));
router.post('/api/sections/activate', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(sectionActivate));
router.put('/api/sections/update', middleware.verifytoken, middleware.isAdmin, makeExpressCallback(sectionUpdate));
router.post('/api/sections/find', middleware.verifytoken, middleware.isNotStudent, makeExpressCallback(fetchSection))







module.exports = router;
