const controllerDeactivateAccount = ({ deactivateAccount }) => {
    return async function updatePass(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            console.log(accountInfo)

            //call in use case function
            const deactivatedAccount = await deactivateAccount(accountInfo.uid);
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(deactivatedAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "account deactivated",
                    accountInfo: {deactivatedAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerDeactivateAccount;