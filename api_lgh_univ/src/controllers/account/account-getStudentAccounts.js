const controllerGetStudentAccounts = ({ getStudentAccounts }) => {
    return async function getStudAccounts(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
            const fetchedStudentAccounts = await getStudentAccounts();

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(fetchedStudentAccounts.modifiedOn).toUTCString(),
                },
                statusCode: 200,
                body: fetchedStudentAccounts
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerGetStudentAccounts;