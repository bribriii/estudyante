const hashPassword = require('../../infra/bcrypt/app');

const controllerUpdateRole= ({ updatePassword }) => {
    return async function updatePass(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            //call in use case function
            const hashedPass = await hashPassword.hash(accountInfo.password.trim());

            const updatedAccount = await updatePassword({
                uid: accountInfo.uid,
                password: hashedPass
            });
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(updatedAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "password updated",
                    accountInfo: {updatedAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerUpdateRole;