const controllerGetAccounts = ({ listAccounts }) => {
    return async function getAccs(httpRequest) {
      try {
        const { source = {}, ...roleInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const posted = await listAccounts();
        return {
          headers: {
            "Content-Type": "application/json",
            "Last-Modified": new Date(posted.modifiedOn).toUTCString()
          },
          statusCode: 200,
          body: posted 
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
  
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  
  module.exports = controllerGetAccounts;