const bcrypt = require('../../infra/bcrypt/app');

const controllerResetPassword = ({ resetPassword }) => {
    return async function resetPass(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            console.log(accountInfo)
            //call in use case function
            const defaultPass = await bcrypt.makeDefaultPass();

            const updatedAccount = await resetPassword({
                uid: accountInfo.uid,
                password: defaultPass
            });
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(updatedAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "password has been reset",
                    accountInfo: {updatedAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerResetPassword;