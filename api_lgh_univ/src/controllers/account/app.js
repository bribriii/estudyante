const controllerGetAccounts = require("./account-getall");
const controllerGetStudentAccounts = require("./account-getStudentAccounts");
const controllerCreateAccount = require("./account-create");
const controllerFindAccount = require("./account-find");
const controllerAuthenticate = require("./account-authenticate");
const controllerUpdatePassword = require("./account-updatePassword");
const controllerResetPassword = require("./account-resetPassword");
const controllerActivateAccount = require("./account-activate");
const controllerDeactivateAccount = require("./account-deactivate");
const controllerUpdateUserRole = require("./acct-updateUserRole");


const {
    listAccounts,
    getStudentAccounts,
    createAccount,
    findAccount,
    authenticate,
    updatePassword,
    resetPassword,
    activateAccount,
    deactivateAccount,
    updateUserRole
} = require("../../use-cases/accounts/app");


const listAccount = controllerGetAccounts({ listAccounts });
const listStudentAccounts = controllerGetStudentAccounts({ getStudentAccounts });
const createNewAccount = controllerCreateAccount({ createAccount });
const fetchAccount = controllerFindAccount({ findAccount });
const authenticateAccount = controllerAuthenticate({ authenticate });
const updateAccountPassword = controllerUpdatePassword({ updatePassword });
const resetAccountPassword = controllerResetPassword({ resetPassword });
const deactivateUserAccount = controllerDeactivateAccount({ deactivateAccount });
const activateUserAccount = controllerActivateAccount({ activateAccount });
const updateRole = controllerUpdateUserRole({ updateUserRole });

const accountController = Object.freeze({
    listAccount,
    listStudentAccounts,
    createNewAccount,
    fetchAccount,
    authenticateAccount,
    updateAccountPassword,
    resetAccountPassword,
    deactivateUserAccount,
    activateUserAccount,
    updateRole
});

module.exports = accountController;
module.exports = {
    listAccount,
    listStudentAccounts,
    createNewAccount,
    fetchAccount,
    authenticateAccount,
    updateAccountPassword,
    resetAccountPassword,
    deactivateAccount,
    deactivateUserAccount,
    activateUserAccount,
    updateRole
}