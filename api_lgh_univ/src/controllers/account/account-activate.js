const controllerActivateAccount = ({ activateAccount }) => {
    return async function updatePass(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            //call in use case function
            const activatedAccount = await activateAccount(accountInfo.uid);
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(activatedAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "account activated",
                    accountInfo: {activatedAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerActivateAccount;