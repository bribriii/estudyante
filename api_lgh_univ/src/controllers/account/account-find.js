const controllerFindAccount = ({ findAccount }) => {
    return async function findAcc(httpRequest){

        try{
            const {source = {}, accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            //retrieve id from params
            const requestedUid = {
                ...accountInfo,
                source,
                uid: httpRequest.params.uid
            }

            //call in use case function
            const fetchedAccount = await findAccount(requestedUid);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(fetchedAccount.modifiedOn).toUTCString(),
                },
                statusCode: 200,
                body: {fetchedAccount}
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerFindAccount;