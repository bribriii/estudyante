const jwt = require('../../infra/jwt/app');

const controllerAuthenticate = ({ authenticate }) => {
    return async function authenticateAccount(httpRequest){

        try{
            const {source = {}, ...accountInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            //retrieve id from params
            const authDetails = {
                ...accountInfo,
                source
            }

            //call in use case function
            const authenticatedAcct = await authenticate(authDetails);
            const token = await jwt.generateToken(authenticatedAcct);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(authenticatedAcct.modifiedOn).toUTCString(),
                },
                statusCode: 200,
                body: {
                    success: true,
                    message: 'authenticated successfully',
                    accountInfo: {
                        uid: authenticatedAcct.uid,
                        roleid: authenticatedAcct.roleid
                    },
                    token: token
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch
    }
}

module.exports = controllerAuthenticate;