const controllerCreateStudent = require('./student-create');
const controllerGetStudents = require('./student-list');
const controllerFindStudent = require('./student-find');
const controllerFindStudentSubjects = require('./student-find-subjects');
const controllerFindStudentGrades = require('./student-find-grades');
const controllerUpdateStudentProfile = require('./student-update-profile');


const {
    createStudent,
    getStudents,
    findStudent,
    findStudentGrades,
    findStudentSubject,
    updateStudentProfile
} = require("../../use-cases/student/app");

const newStudent = controllerCreateStudent({ createStudent });
const listStudents = controllerGetStudents({ getStudents });
const getStudent = controllerFindStudent({ findStudent });
const getStudentGrades = controllerFindStudentGrades({ findStudentGrades });
const getStudentSubjects = controllerFindStudentSubjects({ findStudentSubject });
const updateStudent = controllerUpdateStudentProfile({ updateStudentProfile });

const studentController = Object.freeze({
    newStudent,
    listStudents,
    getStudent,
    getStudentGrades,
    getStudentSubjects,
    updateStudent
});

module.exports = studentController;
module.exports = {
    newStudent,
    listStudents,
    getStudent,
    getStudentGrades,
    getStudentSubjects,
    updateStudent
};