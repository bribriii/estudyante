const controllerUpdateStudentProfile = ({ updateStudentProfile }) => {
    return async function putStudent(httpRequest){
        try {
            // get http request to submitted data
            const { source = {}, ...studentInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-Agent"];
            if (httpRequest.headers["Referer"]) {
              source.referrer = httpRequest.headers["Referer"];
            }
            // end
            const toEdit = {
              sid: httpRequest.params.sid,
              ...studentInfo,
              source,
            }
            const patched = await updateStudentProfile(toEdit);


            return {
              headers: {
                'Content-Type': 'application/json'
              },
              statusCode: 201,
              body: { patched }
            }
            
          } catch (e) {
            // TODO: Error logging
            console.log(e);
      
            return {
              headers: {
                "Content-Type": "application/json"
              },
              statusCode: 400,
              body: {
                error: e.message
              }
            };
          }
    }
}

module.exports = controllerUpdateStudentProfile;