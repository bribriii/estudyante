const generatePassword = require('../../infra/bcrypt/app');
const generateId = require('../../data-access/sequelize-access/account/app');

const controllerCreateStudent = ({ createStudent }) => {
    return async function createAcc(httpRequest){

        try{
            const {source = {}, ...studentInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            const generatedId = await generateId.generateId();
            const generatedPassword = await generatePassword.makeDefaultPass();
            
            const newAcctInfo = {
                ...studentInfo,
                uid: generatedId,
                password: generatedPassword,
                roleid: 1
            }
            //call in use case function
             const createdAccount = await createStudent(newAcctInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(createdAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "new student account created",
                    studentInfo: {createdAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateStudent;