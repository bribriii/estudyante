const controllerFindStudentSubjects = ({ findStudentSubject }) => {
    return async function find(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, ...studentInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...studentInfo,
          source,
          sid: httpRequest.params.sid
        };
        // end here; to retrieve id
        const view = await findStudentSubject(toView.sid);        
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = controllerFindStudentSubjects;
  