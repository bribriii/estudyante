const controllerMakeRole = require("./role-insert");
const controllerListRole = require("./role-getall");
const controllerFindRole = require("./role-find");
const controllerUpdateRole = require("./role-update");
const controllerDeacRole = require("./role-deactivate");

const {
    insertRole,
    listRole,
    findRole,
    updateRole,
    deacRole
}  = require("../../use-cases/roles/app");

const createRole = controllerMakeRole({ insertRole });
const listRoles = controllerListRole({ listRole });
const findRoles = controllerFindRole({ findRole });
const updateRoles = controllerUpdateRole({ updateRole });
const deacRoles = controllerDeacRole({ deacRole });

const roleController = Object.freeze({
    createRole,
    listRoles,
    findRoles,
    updateRoles,
    deacRoles
})

module.exports = roleController;
module.exports = {
    createRole,
    listRoles,
    findRoles,
    updateRoles,
    deacRoles
}