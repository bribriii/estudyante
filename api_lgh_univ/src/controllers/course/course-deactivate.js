const controllerDeactivateCourse = ({ deactivateCourse }) => {
    return async function deactivateCrs(httpRequest){

        try{
            const {source = {}, ...courseInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            const courseid = httpRequest.params.courseid
            //call in use case function
            const deactivated = await deactivateCourse(courseid);
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(deactivated.modifiedOn).toUTCString(),
                },
                statusCode: 202,
                body: {
                    message: "account deactivated",
                    course: {deactivated}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerDeactivateCourse;