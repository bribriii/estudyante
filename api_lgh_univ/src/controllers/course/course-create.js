const controllerCreateCourse = ({ createCourse }) => {
    return async function addCourse(httpRequest){

        try{
            const {source = {}, ...courseInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
             const createdAccount = await createCourse(courseInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(createdAccount.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "course has been added",
                    accountInfo: {createdAccount}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateCourse;