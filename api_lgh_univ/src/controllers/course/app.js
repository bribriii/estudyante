const controllerCreateCourse = require('./course-create');
const controllerGetCourses = require('./course-getAll');
const controllerFindCourse = require('./course-find');
const controllerUpdateCourse = require('./course-update');
const controllerActivateCourse = require('./course-activate');
const controllerDeactivateCourse = require('./course-deactivate');

const {
    createCourse,
    getCourses,
    findCourse,
    updateCourse,
    activateCourse,
    deactivateCourse
} = require('../../use-cases/course/app');

const createNewCourse = controllerCreateCourse({ createCourse });
const listCourses = controllerGetCourses({ getCourses });
const getCourse = controllerFindCourse({ findCourse });
const updateCourseInfo = controllerUpdateCourse({ updateCourse });
const activateCourses = controllerActivateCourse({ activateCourse });
const deactivateCourses = controllerDeactivateCourse({ deactivateCourse }); 

const courseControllers = Object.freeze({
    createNewCourse,
    listCourses,
    getCourse,
    updateCourseInfo,
    activateCourses,
    deactivateCourses
});

module.exports = courseControllers;
module.exports = {
    createNewCourse,
    listCourses,
    getCourse,
    updateCourseInfo,
    activateCourses,
    deactivateCourses
}