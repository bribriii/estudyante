const controllerUpdateCourse = ({ updateCourse }) => {
    return async function put(httpRequest){
        try {
            // get http request to submitted data
            const { source = {}, ...courseInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-Agent"];
            if (httpRequest.headers["Referer"]) {
              source.referrer = httpRequest.headers["Referer"];
            }
            // end
            const toEdit = {
              courseid: httpRequest.params.courseid,
              ...courseInfo,
              source,
            }
            const patched = await updateCourse(toEdit);


            return {
              headers: {
                'Content-Type': 'application/json'
              },
              statusCode: 200,
              body: { patched }
            }
            
          } catch (e) {
            // TODO: Error logging
            console.log(e);
      
            return {
              headers: {
                "Content-Type": "application/json"
              },
              statusCode: 400,
              body: {
                error: e.message
              }
            };
          }
    }
}

module.exports = controllerUpdateCourse;