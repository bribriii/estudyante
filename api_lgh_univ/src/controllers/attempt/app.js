const controllerCreateAttempt = require('./attempt-create');
const controllerCreateAttemptGrade = require('./attempt-encode-grade');
const controllerFindAttempt = require('./attempt-find');
const controllerListAttempts = require('./attempt-list');
const controllerListCurrentAttempts = require('./attempt-list-current');


const {
    createAttemptGrade,
    createNewAttempt,
    listAttempts,
    listCurrentAttempts,
    findAttempt
} = require('../../use-cases/attempt/app');


const createAttempt = controllerCreateAttempt({ createNewAttempt });
const encodeAttemptGrade = controllerCreateAttemptGrade({ createAttemptGrade });
const getAttempts = controllerListAttempts({ listAttempts });
const getCurrentAttempts = controllerListCurrentAttempts({ listCurrentAttempts });
const fetchAttempt = controllerFindAttempt({ findAttempt });


const attemptController = Object.freeze({
    createAttempt,
    encodeAttemptGrade,
    getCurrentAttempts,
    getAttempts,
    fetchAttempt
});

module.exports = attemptController;
module.exports - {
    createAttempt,
    encodeAttemptGrade,
    getCurrentAttempts,
    getAttempts,
    fetchAttempt
}