const controllerCreateAttempt = ({ createNewAttempt }) => {
    return async function add(httpRequest){

        try{
            const {source = {}, ...attemptInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
             const created = await createNewAttempt(attemptInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(created.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "new attempt added has been added",
                    created
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateAttempt;