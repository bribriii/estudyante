const controllerCreateGradeAttempt = ({ createAttemptGrade }) => {
    return async function add(httpRequest){

        try{
            const {source = {}, ...attemptInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
             const created = await createAttemptGrade(attemptInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(created.modifiedOn).toUTCString(),
                },
                statusCode: 200,
                body: {
                    message: "attempt grade has been encoded",
                    created
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateGradeAttempt;