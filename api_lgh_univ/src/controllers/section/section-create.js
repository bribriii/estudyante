const controllerCreateSection = ({ createSection }) => {
    return async function addCourse(httpRequest){

        try{
            const {source = {}, ...sectionInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
             const created = await createSection(sectionInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(created.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "section has been added",
                    created  
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateSection;