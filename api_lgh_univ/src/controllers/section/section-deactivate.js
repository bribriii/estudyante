const controllerDeactivateSection= ({ deactivateSection }) => {
    return async function deactivate(httpRequest){

        try{
            const {source = {}, ...sectionInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            
            //call in use case function
            const deactivated = await deactivateSection(sectionInfo);
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(deactivated.modifiedOn).toUTCString(),
                },
                statusCode: 202,
                body: {
                    message: "section deactivated",
                    course: {deactivated}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerDeactivateSection;