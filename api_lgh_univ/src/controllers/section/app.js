const controllerCreateSection = require('./section-create');
const constrollerActivateSection = require('./section-activate');
const controllreDeactivateSection = require('./section-deactivate');
const controllerUpdateSection = require('./section-update');
const controllerListSections = require('./section-list');
const controllerFindSection = require('./section-find');

const {
    createSection,
    activateSection,
    deactivateSection,
    updateSection,
    listSection,
    findSection
} = require('../../use-cases/section/app');

const createNewSection = controllerCreateSection({ createSection });
const sectionActivate = constrollerActivateSection({ activateSection });
const sectionDeactivate = controllreDeactivateSection({ deactivateSection });
const sectionUpdate = controllerUpdateSection({ updateSection });
const getSections = controllerListSections({ listSection });
const fetchSection = controllerFindSection({ findSection });

const sectionController = Object.freeze({
    createNewSection,
    sectionActivate,
    sectionDeactivate,
    sectionUpdate,
    getSections,
    fetchSection
});

module.exports = sectionController;
module.exports = {
    createNewSection,
    sectionActivate,
    sectionDeactivate,
    sectionUpdate,
    getSections,
    fetchSection
}