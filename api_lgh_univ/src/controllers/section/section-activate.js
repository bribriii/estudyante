const controllerActivateSection = ({ activateSection }) => {
    return async function activateCrs(httpRequest){

        try{
            const {source = {}, ...sectionInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }

            
            //call in use case function
            const activated = await activateSection(sectionInfo);
            //end use case execution

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(activated.modifiedOn).toUTCString(),
                },
                statusCode: 200,
                body: {
                    message: "section activated",
                    course: {activated}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerActivateSection;