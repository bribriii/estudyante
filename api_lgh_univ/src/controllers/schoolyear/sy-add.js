const controllerCreateSy = ({ createSy }) => {
    return async function add(httpRequest){

        try{
            const {source = {}, ...syInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            //call in use case function
             const created = await createSy(syInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(created.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "course has been added",
                    accountInfo: {created}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch

    }
}

module.exports = controllerCreateSy;