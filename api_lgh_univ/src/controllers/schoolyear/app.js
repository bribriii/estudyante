const controllerCreateSy = require('./sy-add');
const controllerListSy = require('./sy-list');
const controllerFindSy = require('./sy-find');


const {
    listSy,
    createSy,
    findSy
} = require('../../use-cases/schoolyear/app');


const createNewSy = controllerCreateSy({ createSy });
const getAllSy = controllerListSy({ listSy });
const getSy = controllerFindSy({ findSy });

const syControllers = Object.freeze({
    createNewSy,
    getAllSy,
    getSy
})

module.exports = syControllers;
module.exports = {
    createNewSy,
    getAllSy,
    getSy
}