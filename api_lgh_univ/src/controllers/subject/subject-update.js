const controllerUpdateSubject = ({ updateSubject }) => {
    return async function put(httpRequest){
        try {
            // get http request to submitted data
            const { source = {}, ...subjectInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-Agent"];
            if (httpRequest.headers["Referer"]) {
              source.referrer = httpRequest.headers["Referer"];
            }
            // end
            const toEdit = {
              subjectid: httpRequest.params.subjectid,
              ...subjectInfo,
              source,
            }
            const patched = await updateSubject(toEdit);


            return {
              headers: {
                'Content-Type': 'application/json'
              },
              statusCode: 200,
              body: { patched }
            }
            
          } catch (e) {
            // TODO: Error logging
            console.log(e);
      
            return {
              headers: {
                "Content-Type": "application/json"
              },
              statusCode: 400,
              body: {
                error: e.message
              }
            };
          }
    }
}

module.exports = controllerUpdateSubject;