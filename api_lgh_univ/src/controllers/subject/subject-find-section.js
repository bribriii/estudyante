const controllerFindSubjectSection = ({ findSubjectSections }) => {
    return async function find(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, ...subjectInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...subjectInfo,
          source,
          subjectid: httpRequest.params.subjectid
        };
        console.log(toView.subjectid)
        // end here; to retrieve id
        const view = await findSubjectSections(toView.subjectid);        
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = controllerFindSubjectSection;
  