const controllerCreateSubject = ({ createSubject }) => {
    return async function createSubj(httpRequest){
        try{
            const {source = {}, ...subjectInfo } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers["User-agent"];

            if(httpRequest.headers["Referer"]){
                source.referrer = httpRequest.headers["Referer"];
            }
            
            
            const newSubjectInfo = {
                ...subjectInfo,
            }
            
            //call in use case function
            const createdSubject = await createSubject(newSubjectInfo);

            return {
                headers: {
                    "Constent-Type" : "application/json",
                    "Last-Modified" : new Date(createdSubject.modifiedOn).toUTCString(),
                },
                statusCode: 201,
                body: {
                    message: "new student account created",
                    subjectInfo: {createdSubject}
                }
            }

        }catch(err){
            console.log(err);
            return {
                headers: {
                    "Constent-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            };//end return
        };//end catch
    }
}

module.exports = controllerCreateSubject;