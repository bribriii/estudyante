const controllerCreateSubject = require('./subject-create');
const controllerUpdateSubject = require('./subject-update');
const controllerActivateSubject = require('./subject-activate');
const controllerDeactivateSubject = require('./subject-deactivate');
const controllerListSubjects = require('./subject-list');
const controllerFindSubject = require('./subject-find');
const controllerFindSubjectSection = require('./subject-find-section');

const {
    createSubject,
    findSubject,
    findSubjectSections,
    listSubjects,
    deactivateSubject,
    activateSubject,
    updateSubject
} = require('../../use-cases/subject/app');

const addSubject = controllerCreateSubject({ createSubject });
const getSubject = controllerFindSubject({ findSubject });
const getSubjectSections = controllerFindSubjectSection({ findSubjectSections });
const getSubjects = controllerListSubjects({ listSubjects });
const deactivateSubj = controllerDeactivateSubject({ deactivateSubject });
const activateSubj = controllerActivateSubject({ activateSubject });
const updateSubj = controllerUpdateSubject({ updateSubject });


const subjectController = {
    addSubject,
    getSubject,
    getSubjectSections,
    getSubjects,
    deactivateSubj,
    activateSubj,
    updateSubj
}


module.exports = subjectController;
module.exports = {
    addSubject,
    getSubject,
    getSubjectSections,
    getSubjects,
    deactivateSubj,
    activateSubj,
    updateSubj
}